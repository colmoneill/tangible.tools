#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u"Colm O'Neill"
SITENAME = u'tangible.tools'
SITEURL = 'http://tangible.tools'

PATH = 'content'
STATIC_PATHS = ['images']
OUTPUT_PATH = 'output'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'en'
DEFAULT_DATE_FORMAT = '%d/%m/%Y'
DATE_FORMATS = {
    'en': '%d/%m/%Y',
}

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_CATEGORY = 'All'
DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_PAGES_ON_MENU = False

MENUITEMS = (('about this site', '/pages/about-this-site.html'),
             ('inventory', '/pages/inventory.html'),
             ('teachings', '/category/teaching.html'),)

THEME = "theme/tangible.tools.theme"
CSS_FILE = 'screen.css'

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

PLUGIN_PATHS = ['plugins/pelican-toc', 'plugins/neighbors', 'plugins/simple_footnotes']
PLUGINS = ['toc', 'neighbors', 'simple_footnotes']
