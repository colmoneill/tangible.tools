Title: Colour theory
Date: 20/10/2019
Template: slidy
Status: hidden
Tags: resource

<div class="slide section level1">

<p>Title: Colour theory Date: 20/10/2019 Template: slidy Status: draft Tags: resource</p>
<!--

pandoc resource_colour_theory.md --from=Markdown --to=Slidy -o resource_colour_theory.md.slidy.md

paste to head of new doc:
Title: Colour theory
Date: 20/10/2019
Template: slidy
Status: published
Tags: resource

-->
</div>
<div id="colour-theory" class="slide section level1">
<h1>Colour theory</h1>
</div>
<div id="initial-question" class="slide section level1">
<h1>initial question:</h1>
<p>What is colour ?</p>
</div>
<div id="colour-is-light" class="slide section level1">
<h1>Colour is light</h1>
<p>Light is a electromagnetic radiation, meaning, on it’s own it is invisible, we can use tools to measure it, but to our eyes, light is invisible. We see the effects of light.</p>
</div>
<div id="colour-is-a-fragment-of-daylight" class="slide section level1">
<h1>Colour is a fragment of daylight</h1>
<p><img src="images/colour-theory/Light_dispersion_conceptual_waves.gif" style="width:100%;"></p>
</div>
<div id="section" class="slide section level1">
<h1><img src="images/colour-theory/prism.jpg" style="width:100%;"></h1>
</div>
<div id="section-1" class="slide section level1">
<h1><img src="images/colour-theory/wavelength_graph.jpg" style="width:100%;"></h1>
<p><img src="https://upload.wikimedia.org/wikipedia/commons/c/c4/Rendered_Spectrum.png" style="width:100%;"></p>
</div>
<div id="colour-is-our-eyes-reading-all-of-the-wavelengths-that-are-present-in-light." class="slide section level1">
<h1>Colour is our eyes reading all of the wavelengths that are present in light.</h1>
<p>Our eyes are full of various receptors that enable us to see colour</p>
<p><img src="images/colour-theory/human-eye-cells.jpg" style="width:100%;"></p>
</div>
<div id="rods-cones" class="slide section level1">
<h1>Rods &amp; cones</h1>
<p>Both of these are what we call photoreceptor cells, meaning, sensitive to light.</p>
<ul>
<li><p>Rods are most numerous, about 120 million of them per eye, they are more <em>sensitive</em> to light than cones, but not sensitive to colours. They give us the ability to see in low light.</p></li>
<li><p>Cones are counted in the 6 million per eye, they are the cells that let us see various colours in the day time. They are sensitive to different wavelengths.</p></li>
</ul>
</div>
<div id="we-only-see-the-effects-of-light." class="slide section level1">
<h1>We only see the effects of light.</h1>
<p>What we see with our cognitive brain is light hitting a surface.</p>
<p>This is what we call <em>additive theory</em>. It can also be called <em>light theory</em>, it deals with radiated and filtered light.</p>
</div>
<div id="primary-colours-in-additive-light-theory" class="slide section level1">
<h1>Primary colours in additive light theory:</h1>
<ul>
<li>Red</li>
<li>Green</li>
<li>Blue</li>
</ul>
</div>
<div id="there-is-a-second-colour-theory" class="slide section level1">
<h1>There is a second colour theory</h1>
<p>What happens if you mix Red, Green and Blue paint together ?</p>
</div>
<div id="the-second-theory-deals-with-how-light-is-absorbed" class="slide section level1">
<h1>The second theory deals with how light is absorbed</h1>
<p>it is called <em>subtractive theory</em> and can also be called <em>pigment theory</em> because we are dealing with pigments of colour, absorbing certain wavelengths of the light spectrum</p>
<p><img src="images/colour-theory/additive-and-subtractive.jpg" style="width:100%;"></p>
</div>
<div id="section-2" class="slide section level1">
<h1><img src="images/colour-theory/colorbasics001.gif" style="width:100%;"></h1>
</div>
<div id="section-3" class="slide section level1">
<h1><img src="images/colour-theory/CMYK-color-combinations.png" style="width:100%;"></h1>
</div>
<div id="additive-vs-subtractive" class="slide section level1">
<h1>Additive vs Subtractive</h1>
<ul>
<li><p>Additive colour theory deals with how we see light and is used on screens, in video and photography</p></li>
<li><p>Subtractive colour theory deals with how light is reflected off of pages, canvases or card and is used in printing and painting.</p></li>
</ul>
</div>
<div id="screens-use-rgb" class="slide section level1">
<h1>Screens use RGB</h1>
<p><img src="images/colour-theory/lcd-pixel-lg.png" style="width:100%;"></p>
</div>
<div id="section-4" class="slide section level1">
<h1><img src="images/colour-theory/lcd-pixel.jpg" style="width:100%;"></h1>
</div>
<div id="section-5" class="slide section level1">
<h1><img src="images/colour-theory/macro-screen-lcd.jpg" style="width:100%;"></h1>
</div>
<div id="section-6" class="slide section level1">
<h1><img src="images/colour-theory/lcd-type.jpg" style="width:100%;"></h1>
</div>
<div id="in-graphics-practice-we-need-to-be-able-to-codify-colours" class="slide section level1">
<h1>In graphics practice, we need to be able to codify colours</h1>
<p>We need colour codes to be able to communicate colours between people and software but also between designers and printers</p>
<p>There are two basic colour systems to reflect both the additive and subtractive</p>
<ul>
<li>RGB for screen use, web use, video and photography use</li>
</ul>
<p>but as soon as you plan on printing your documents on screen, you must switch to</p>
<ul>
<li>CMYK for inks and paints.</li>
</ul>
</div>
<div id="rgb-cmyk" class="slide section level1">
<h1>RGB &amp; CMYK</h1>
<ul>
<li><p>RBG = Red, Blue, Green</p></li>
<li><p>CMYK = Cyan, Magenta, Yellow, K: for Black</p></li>
</ul>
<p>(K is for Key, used instead of B for Black, because in some printing cases, we use a 4th or even 5th colour instead of Black for Key reference)</p>
</div>
<div id="general-terms-to-describe-colours" class="slide section level1">
<h1>general terms to describe colours</h1>
<ul>
<li>Hue</li>
<li>Saturation / chroma</li>
<li>Value / Luminosity</li>
</ul>
</div>
<div id="hue" class="slide section level1">
<h1>Hue:</h1>
<p>The property of colors by which they can be perceived as ranging from red through yellow, green, and blue, as determined by the dominant wavelength of the light.</p>
<p><img src="images/colour-theory/hue1.jpg" style="width:100%;"></p>
</div>
<div id="section-7" class="slide section level1">
<h1><img src="images/colour-theory/hue2.jpg" style="width:100%;"></h1>
</div>
<div id="section-8" class="slide section level1">
<h1><img src="images/colour-theory/hue3.jpg" style="width:100%;"></h1>
</div>
<div id="saturation-chroma" class="slide section level1">
<h1>Saturation / chroma:</h1>
<p>The vividness of a color’s hue. Saturation measures the degree to which a color differs from a gray of the same darkness or lightness.</p>
<p><img src="images/colour-theory/saturation1.jpg" style="width:100%;"></p>
<p><img src="images/colour-theory/saturation2.jpg" style="width:100%;"></p>
<p><img src="images/colour-theory/saturation3.jpg" style="width:100%;"></p>
</div>
<div id="value-luminosity" class="slide section level1">
<h1>Value / luminosity</h1>
<p>Attribute of visual sensation according to which an area appears to emit more or less light. Lightness, more or less white in the color composition.</p>
<p><img src="images/colour-theory/value1.jpg" style="width:100%;"></p>
<p><img src="images/colour-theory/value2.jpg" style="width:100%;"></p>
<p><img src="images/colour-theory/value3.jpg" style="width:100%;"></p>
</div>
<div id="setting-colours-in-software" class="slide section level1">
<h1>Setting colours in software</h1>
<p><img src="images/colour-theory/color_picker2.png" style="width:100%;"></p>
<p><img src="images/colour-theory/color_picker3.png" style="width:100%;"></p>
</div>
<div id="the-colour-wheel" class="slide section level1">
<h1>the colour wheel</h1>
<p><img src="images/colour-theory/Color-Wheel-20.gif" style="width:100%;"></p>
</div>
<div id="colour-formulas" class="slide section level1">
<h1>colour formulas</h1>
<ul>
<li>Complementary colours</li>
<li>Analogous colours</li>
<li>Triadic colours</li>
</ul>
</div>
<div id="complementary" class="slide section level1">
<h1>Complementary</h1>
<p><img src="images/colour-theory/Complementary-3-column.png" style="width:100%;"></p>
</div>
<div id="analogous" class="slide section level1">
<h1>Analogous</h1>
<p><img src="images/colour-theory/Analogous-3-column.png" style="width:100%;"></p>
</div>
<div id="triadic" class="slide section level1">
<h1>Triadic</h1>
<p><img src="images/colour-theory/Triadic-3-column.png" style="width:100%;"></p>
</div>
<div id="other-key-notions-to-describe-colour" class="slide section level1">
<h1>Other key notions to describe colour:</h1>
<p><img src="images/colour-theory/Warm-colors-2-column.png" style="width:100%;"></p>
<p><img src="images/colour-theory/Cool-colors-2-column.png" style="width:100%;"></p>
<p><img src="images/colour-theory/Monochromatic-2-column.png" style="width:100%;"></p>
<p><img src="images/colour-theory/Grayscale-2-column.png" style="width:100%;"></p>
</div>
<div id="useful-tools" class="slide section level1">
<h1>useful tools</h1>
<p><a href="https://color.adobe.com/create/color-wheel" class="uri">https://color.adobe.com/create/color-wheel</a> <br><a href="https://colorpalettes.net/" class="uri">https://colorpalettes.net/</a> <br><a href="https://www.color-hex.com/color-palettes/" class="uri">https://www.color-hex.com/color-palettes/</a> <br><a href="https://colorhunt.co/" class="uri">https://colorhunt.co/</a></p>
</div>
<div id="recap" class="slide section level1">
<h1>recap</h1>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/_2LLXnUdUIc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
</iframe>
</div>
