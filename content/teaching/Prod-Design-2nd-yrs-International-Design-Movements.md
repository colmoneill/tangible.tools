Date: 2019/09/02
Title: International Design Movements

#### BA Product design innovation

## Course schedule:
* Mondays from 12.00 → 13.00 with Colin Deevy
* Fridays from 9.00 → 11.00 with Colm O'Neill (room D516)

## Week 1 — 20/09/2019

Design does not happen in a vacuum. The expansion and diversification of design movements over the last 120 years is the subject of this module but studying these movements one by one does not give an accurate picture of the times they emerged from. This module aims to expand our understanding of the history of design and the effects that historic design practices and aesthetics have had on our field then and today.

The International Design Movements module will take the shape of a year long research project, throughout which, as a group, we will be establishing a multi-layered timeline of the 20th and 21st century. Before placing and studying design movements, we must understand the socio-political **contexts** from which they emerged.

The span of the timeline will be divided into sections that will be assigned to student groups. At least three common general research strands will be agreed upon, with the group, to enable a degree of continuity between each groups work. These strands could be for example: social context, economical context, industrial context.

Once a contextual baseline is established we will position and research designers reactions to the contexts they emerged from and place the following movements:
<br>Werkbund, Italian Futurism, Russian Constructivism, De Stijil, Bauhaus, Eileen Grey, International Style. American, Scandanavian, Italian, Organic Design, Pop Culture (Itally/Germany), Memphis (Studio Archimia), Sustainability, Cradle to Cradle, Irish Arts & Crafts, Scandanavian Report, Kilkenny Design Workshop and design in the 21st Century

### Timeline examples:

* <https://www.bbc.com/timelines/zp7bgk7#zwcthyc>
* <https://vimeo.com/67015825>
* <https://www.tiki-toki.com/timeline/entry/302294/History-of-Graphic-Design-Timeline/#vars!date=0784_BC-01-12_18:00:14!> (open in Chrome)
* <https://thehistoryoftheweb.com/timeline/?date_from=1994&date_to=1996>
* <https://vanabbemuseum.nl/en/programme/programme/contexts/>

### Research methodology example:
* <http://www.wordsinspace.net/designingmethods/spring2018/category/methods-toolkit/>
* <https://en.wikipedia.org/wiki/1910s> is very different from <https://fr.wikipedia.org/wiki/Ann%C3%A9es_1910> so please consider the bias of the sources you choose to study your period.

## Week 2 — 27/09/2019

Groups and periods:
* 1900 → 1918 Evan, Evan + Emily
* 1918 → 1940 Luke, Fredo, Alex + possible 4th student
* 1940 → 1960 Sorcha (spelling ?), Genetta (spelling ?), Rachel
* 1960 → 1975 Claudia, Maeve, Eamon
* 1975 → 1990 TBD
* 1990 → present times TBD
