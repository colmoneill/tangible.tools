Title: Entrepreneurial Innovation
Date: 10/2019
Template: slidy
Status: hidden
Tags: resource

<div class="slide section level1">

<p>Title: Entrepreneurial Innovation Date: 10/2019 Template: slidy Status: draft</p>
<!--

pandoc resource_Entrepreneurial_innovation.md --from=Markdown --to=Slidy -o resource_Entrepreneurial_innovation.md.slidy.md

paste to head of new doc:
Title: Entrepreneurial Innovation
Date: 10/2019
Template: slidy
Status: hidden
Tags: resource

-->
</div>
<div id="entrepreneurial-innovation" class="slide section level1">
<h1>Entrepreneurial Innovation</h1>
</div>
<div id="outline" class="slide section level1">
<h1>outline</h1>
<p>Before deciding <em>how to</em> innovate, let’s investigate <em>whether</em> it’s worth innovating with your current idea.</p>
<p>The following is a set of methods for thinking and questioning one’s current or soon to be position in the market place.</p>
</div>
<div id="customer-needs" class="slide section level1">
<h1>Customer needs</h1>
<p>Entrepreneurial innovation is often helped by thinking about customer needs.</p>
</div>
<div id="ds-structure" class="slide section level1">
<h1>4 D’s structure</h1>
<p>Entrepreneurial innovation can be prepared using a 4 step method:</p>
<ul>
<li><strong>Discover</strong> : Challenging the problem</li>
<li><strong>Define</strong> : Mapping your Journey</li>
<li><strong>Develop</strong> : Finding the Evidence</li>
<li><strong>Deliver</strong> : Creating Value</li>
</ul>
</div>
<div id="discover" class="slide section level1">
<h1>1 Discover</h1>
<p>A great exercise is to think of an enterprises’ or start ups’ value proposition. Your value proposition should address what market you are targeting, what product or service you are delivering, how you are delivering it and why.</p>
</div>
<div id="building-blocks-to-value-proposition" class="slide section level1">
<h1>Building blocks to Value Proposition:</h1>
<p>There are three building blocks to question then build a strong value proposition:</p>
<ul>
<li>connect with customers</li>
<li>differentiate from alternatives</li>
<li>validate your claims</li>
</ul>
</div>
<div id="challenging-the-problem" class="slide section level1">
<h1>Challenging the problem</h1>
<p>We now must cross the three building blocks with three processes:</p>
<ul>
<li>Generating</li>
<li>Delivering</li>
<li>Reducing</li>
</ul>
</div>
<div id="section" class="slide section level1">
<h1><img src="images/entrepreneurial-innovation/value-prop-blocks.png" style="width:100%;"></h1>
</div>
<div id="the-aim-of-this-chart" class="slide section level1">
<h1>The aim of this chart</h1>
<p>The aim of value proposition chart is to answer the question: ‘Why should we purchase your offering ?’. All of these questions let us identify if there are any weaknesses in our position and doings on the market.</p>
<p>It is a powerful tool to test your enterprise idea, or current enterprise practice. Your value proposition must be strong, across the board for you to make the most of your market. It is also a tool for motivating change in an existing organisation.</p>
</div>
<div id="if-you-know-what-your-customers-want-you-can-deliver-what-they-need." class="slide section level1">
<h1>If you know what your customers want, you can deliver what they need.</h1>
</div>
<div id="define-the-pursuit-of-a-customer" class="slide section level1">
<h1>2 DEFINE : The pursuit of a customer:</h1>
<p>It all starts with understanding who your customer is and <em>really</em> getting to know them.</p>
</div>
<div id="the-pursuit-of-a-customer-is-a-journey" class="slide section level1">
<h1>The pursuit of a customer is a journey</h1>
<p>Simply identifying a product, service or method won’t be enough enough; we need to understand :</p>
<ul>
<li>Aware: How does the customer become aware of you and your products</li>
<li>Engage: How can your customer contact you and start the buying process</li>
<li>Use: How can they buy and receive your product or service</li>
<li>Continue: What happens after they buy from you, how do they continue their buying relationship with you ? (includes customer services, complaint and recovery)</li>
<li>Leave: How does a customer leave you</li>
</ul>
</div>
<div id="understanding-the-customer-journey" class="slide section level1">
<h1>Understanding the customer journey</h1>
<p>Once you understand YOUR customers journey, you can start to understand how you are different from your competitors. Once you know that, we are nearing an understanding of what your Unique selling point is.</p>
</div>
<div id="unique-selling-point" class="slide section level1">
<h1>Unique Selling Point</h1>
<p>is something that differentiates your product or service from your competitors</p>
</div>
<div id="develop" class="slide section level1">
<h1>3 DEVELOP :</h1>
<p>It is now time to start thinking about what your really know about your customers. We know that entrepreneurs have a reputation for being risk takers, but actually, they know their evidence inside and out.</p>
</div>
<div id="finding-the-evidence-for-your-target-market" class="slide section level1">
<h1>Finding the evidence for your target market</h1>
<p>First think about one customer segment. That segment could be as small as one individual, but you really must know this individual inside and out. It is best if we can think about multiple different customer segments and confront each segment to the following claims.</p>
</div>
<div id="method-for-finding-evidence" class="slide section level1">
<h1>Method for finding evidence :</h1>
<p>Question each following claim, and try to score them with ‘High’, ‘Medium’ or ‘Low’, plus back this up with evidence</p>
</div>
<div id="your-ability-to-access-key-decision-makers" class="slide section level1">
<h1>… your ability to access key decision-makers:</h1>
<p>Do you have the ability to access key decision-makers ? Will your communications, marketing and publicity reach decision makers in the target sector that you want ?</p>
<p>‘High’, ‘Medium’ or ‘Low’ ?</p>
</div>
<div id="the-product-service-fit-with-customer-requirement" class="slide section level1">
<h1>… the product / service ‘fit’ with customer requirement</h1>
<p>Does your product really fit with the customer requirement ? Do you know the requirements ? Do you have their specification ? Have you interviewed or gathered feedback ?</p>
</div>
<div id="the-readiness-of-customers-to-change-to-your-solution" class="slide section level1">
<h1>… the readiness of customers to change to your solution</h1>
<p>Are your potential customers ready to change to your solution ? How can you be sure ? Where is your evidence ?</p>
</div>
<div id="your-ability-to-influence-customers-to-change" class="slide section level1">
<h1>… your ability to influence customers to change</h1>
<p>Are your target customers willing to be influenced by you into changing to your product / service ?</p>
</div>
<div id="section-1" class="slide section level1">
<h1><img src="images/entrepreneurial-innovation/finding-evidence.png" style="width:100%;"></h1>
</div>
<div id="deliver" class="slide section level1">
<h1>4 DELIVER</h1>
<p>Think about the value you are adding. This can be adding value to the business (if it exists already and you are innovating within a established context), and / or value to the customer.</p>
</div>
<div id="business-growth" class="slide section level1">
<h1>Business growth</h1>
<p>Business growth is often described as a life cycle.</p>
</div>
<div id="section-2" class="slide section level1">
<h1><img src="images/entrepreneurial-innovation/business-lifecycle.png" style="width:100%;"></h1>
</div>
<div id="to-remain-relevant" class="slide section level1">
<h1>To remain relevant</h1>
<p>a company needs to constantly check-in with their customers and with their strategy making sure they match, and making sure they are adding maximum value to both.</p>
</div>
