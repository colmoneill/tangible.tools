Date: 2019/09/02
Title: Design Practice & Professional Development

#### BA Product design innovation

## Course schedule:
* Thursdays from 14.00 → 16.00 (room D413)

### Week 1 — 2019_09_19 — module introduction
module outline and course development  

### Week 2 — 2019_09_26 — body language

Load this link for a list of questions we will try to answer together <https://annuel.framapad.org/p/design-practice-professional-development?lang=en>

<div style="max-width:854px"><div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://embed.ted.com/talks/amy_cuddy_your_body_language_shapes_who_you_are" width="854" height="480" style="position:absolute;left:0;top:0;width:100%;height:100%" frameborder="0" scrolling="no" allowfullscreen></iframe></div></div>

### Unviversal communication preparation method:

* What
* How
* Why

### First assignment:

Each learner will give a 5 minute presentation on a subject of their choice. This first presentation will not employ any visual supports, meaning no powerpoint or images on a screen. You will select a subject and submit it to the course instructor for validation. Prepare your presentation using the What How Why method, and decide how much time you want to allocate to each section of your presentation.
This presentation will be video taped for you to review and develop a self critique of your presentations.

### Week 3 — 2019_10_03

#### — lessons in body language

#### — video extract from unfortunate debate intervention in american public hearing. 

<!--https://youtu.be/dzm6WvLR_cY?t=106-->

#### — projection
  * posture: head and neck centrally aligned with the rest of your body, be stable on your feet
  * stiffness in the neck or the back, shoulder position
  * breath: a balance, how much breath do you need for each set of sentences that you will deliver
  * your pitch: not too high, not too low, your natural voice tone for maximum projection
  * head resonance: to hear yourself, but also as an amplifying device. It gives a nice ringing quality to your voice, and allows your voice to carry.
  * articulation precision: good mouth articulation, making sure that you are very clear and that you are understood. Emphasise sounds and extremes of words.
  * speed of delivery: keep things nice and slow, allow for pauses, it's a lot easier to understand you at a normal speed. It also helps to project the voice.
  * do not force your voice, do not strain, try to find a comfortable voice volume that you can sustain. Speak from your diaphragm, not your larynx

#### — assignment
