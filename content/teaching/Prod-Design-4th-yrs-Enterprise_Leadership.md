Date: 2019/09/02
Title: Enterprise & Leadership

#### BA Product design innovation

## Course schedule:
* Fridays from 12.00 → 13.00 (room D516)

<!--
Module Aim:
The aim of this module is to introduce learners to the professional/industry element of the business environment when creating a design practice, a business plan, commercialisation opportunity or research development. It aims to equip them with the skills required to function in the commercial environment successfully and to develop an understanding of design entrepreneurship. The learner will be introduced to the role of leadership within design enterprises. It will encourage the learner to investigate options when forming a design enterprise and the skills require to function as a designer with their own business.

### LOs
LO1 Identify a suitable business type for the formation of a design enterprise around HDP (Honors Degree project)

LO2 Understanding the importance of a business, commercialisation & research plan and application of a business model

LO3 Examine and assess the options available for the financing of a design enterprise

LO4 Understanding the use of marketing and leadership strategies for design enterprises

LO5 Distinguish the various uses and types of intellectual property

LO6 Identify a range of opportunities for commercialisation or research development

LO7 To review performance over the course of the module and deliver a personal development plan

### Indicative Content
#### Leadership
Learners engage in an applied collaborative project with a heterogeneous group to develop their skills and understanding of the role of the
leader. Learners are assigned roles of director, team leader or creative director and then engage, at the appropriate capacity, with a project
team. Director Role: (set objectives, co-ordinate & motivate management teams, communicate, hold meetings & report to CEO, provide
support training, report individual performance engagement, outcomes and recommend review). Team Leader Role: (co-ordinate & manage
project team, set objectives & plan project, communicate, hold meetings & maintain records, motivate team & engage in conflict resolution,
report project outcomes, report performance) Creative Director Role: (mentor & support team in research and process, communicate with
team, provide technical guidance & support, motivate team & deliver to stated standards).

### Development Opportunities
Learners are introduced to various business types and will identify the characteristics and suitability for the formation of a design enterprise.
They will engage in research to identify the current state of the design enterprise environment and the role of design leadership. Learners
will explore competitive analysis of design artifact & enterprise and identify the unique selling proposition of their design proposal. Learners
will deliver a development plan for the HDP. Learners will identify between business development, commercialisation or futher research
opportunity, which approach suits their output from the Honours Degree Project

### Financing Opportunities
Learners will gain an understanding of the different types of financing opportunities, funding bodies & stakeholders associated with business
development, commercialisation and further research. (viability, stakeholder analysis, impact, funding strategies, intellectual property,
negotiation skills and language)
-->


### Week 1: 19/09/2019 module introduction

Third level learning is not simply a graded measurement of unitary sets of knowledge, singular skills acquired or learned behaviours. A modern graduate is a thinker and active agent, engaged & responsible in developing new ways to support industry, develop strategic policy & impact society positively. Leadership is a discrete skill that can be learned and this programme aims to support your development and awareness of leadership.

To do this, the learner must engage in reflective practice and become aware of WHAT, for WHOM and WHY they are doing something, and HOW, WHERE, WHEN or with WHOM they are they engaging? More importantly, the graduate needs to become personally responsible for the impact of their decision actions and interactions with others.

### Week 2: 26/09/2019

#### Questionnaire

<iframe src="https://annuel.framapad.org/p/2019_2020-Enterprise-Leadership_module"></iframe>

<https://annuel.framapad.org/p/2019_2020-Enterprise-Leadership_module>

send your answers to me via [colm.oneill@itcarlow.ie](mailto:colm.oneill@itcarlow.ie)

#### Diversifying the course content with guest lecturers

Four to five ideas of guest lecturers have come to mind:

* Cathal Redmond, a Dyson award runner up in 2015, graduate from UL in product design, who is currently studying at ITCarlow in the Supply Chain Management Masters.

* Eric Schrijver, author of Copy This Book <https://copy-this-book.eu/> (based in Belgium so it could be a remote Skype guest presence) a researcher on Copyright, intellectual property and licensing across the board.

* Emma Lucy O'Brien (and / or) David Moore, respectively CEO and Development officer at [VISUAL](visualcarlow.ie), who I would like to ask (later in the semester) to speak about their usage of the Erasmus+ and [AHEH](https://www.artshumanitieshub.eu/) funding they received, in developing a project called THRIVE, a guide for graduates in the Humanities to develop entrepreneurial practices.

* Silvio Lorusso, designer, researcher and author, mostly on the topic of the [entreprecariat](http://networkcultures.org/entreprecariat/what-is-the-entreprecariat/) <https://silviolorusso.com/work/entreprecariat/> & <http://networkcultures.org/entreprecariat/>

* Brian Ogilvie <https://www.itcarlow.ie/research/researchers/meet-researchers-o-z/brian-ogilvie.htm>

### 2019_11_7 Module resources, slides and structures:

1. [Entrepreneurial innovation](/entrepreneurial-innovation.html)
2. [Market validation](/market-validation.html)
3. [Intellectual property](/intellectual-property.html)

### 2019_11_21 The LEAN Canvas

* [lean-canvas-with-details.pdf](images/entrepreneurial-innovation/lean-canvas-with-details.pdf)
* [lean-canvas-with-details.svg](images/entrepreneurial-innovation/lean-canvas-with-details.svg)
* [lean-canvas-no-details.pdf](images/entrepreneurial-innovation/lean-canvas-no-details.pdf)
* [lean-canvas-no-details.svg](images/entrepreneurial-innovation/lean-canvas-no-details.svg)
