Title: Typography
Date: 09/2019
Template: slidy
Status: draft

<!--

pandoc resource_typography.md --from=Markdown --to=Slidy -o resource_typography.md.slidy.md

paste to head of new doc:
Title: Typography
Date: 09/2019
Template: slidy
Status: published

-->

# Typography — a starter resource

# a brief history of type
<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/65353988" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

#
![](https://practicaltypography.com/images/roadsign-standard.jpg)
<img src="https://practicaltypography.com/images/roadsign-standard.jpg"/>
<img src="https://practicaltypography.com/images/roadsign-script.jpg"/>
