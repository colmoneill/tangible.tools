Date: 2019/09/02
Title: Marketing Communications in a Digital Era

#### MSc Digital Marketing

## Course schedule:
* Thursdays from 9.00 → 11.00 (room A210)

## Week 1: 2019_09_19 — module introduction

<!--
### from the Module details

#### Teaching & Learning Strategies:

Learners will be exposed to theory and practice of digital marketing through exposure to and study of academic materials (texts, journals etc.) and industry practice materials (web resources, case studies, live case examples, industry reports etc.). Classes will be tutorial in nature where learners will be expected to actively engage with module materials (online & offline), problem solving exercises and participate in class discussion and debate. Learners will be encouraged to self-reflect Computer Laboratories – instruction classes will take place in computer labs to facilitate digital marketing practice.
-->

### Module Aim:

The aim of this module is to provide learners with an understanding of digital marketing communications theory and practice and to critically evaluate, create and execute a digital communications plan.

<!--
#### Learning Outcomes

On successful completion of this module the learner should be able to:

* LO1: Critically reflect on and evaluate digital marketing communication strategy and practice
* LO2:  To analyse social media marketing it’s benefits, growth and evolution. To critically assess the different social channels and to configure social media business accounts and content across various social platforms.
* LO3: To deliberate and evaluate best practice in search marketing, inbound and outbound content strategy and content management systems
* LO4: To explore social listening tools and manage online brand reputation.
* LO5: To plan and execute a digital marketing communications campaign determining the best media mix to meet objectives. To select online and offline tools to maximize the return on investment.

#### Indicative Content
* Introduction to Marketing Communications: <br>
Marketing Communications - an introduction. Planning for marketing communications. Online and offline Marketing communication strategy
* Digital Marketing Communications Strategy: <br>
The changing mix of marketing communications from traditional media to online media. Develop an integrated marketing communications strategy that combines all forms of communication for optimal effect.
* Social Media Strategy: <br>
Assessing the social media environment; social media goals and establish key performance indicators; create a social media content strategy; social media policy; understand the ethics of social media marketing.
* Social Media Platforms: <br>
Configure and manage social channels such as Facebook for Business Twitter Blog Linkedin Google+ Youtube Pinterest and any Emergent Channels Operate a social media content management system Email Marketing.
* Content Marketing Solutions: <br>
Inbound and Outbound Content Strategy; Email Marketing; Blogs
* Digital Display Advertising: <br>
Banner and Display advertising Pay Per Click (PPC) Social Media Advertising Ad formats and features. Campaign planning; targeting and tracking campaign; optimising the campaign and campaign Budget
* Online PR and Brand Management: <br>
Social Listening Tools; Online PR and brand management
* Social and Ethical Issues in Digital Marketing Communications:
Ethics, social and legal issues pertaining to digital communications
* Digital Marketing Communications Plan: <br>
The Digital Marketing Communications Plan

-->

### Partial subject outlines:

Form of communication in digital marketing:
<br>— SEO, SEM, content marketing, influencer marketing, content automation, campaign marketing, data-driven marketing, e-commerce marketing, social media marketing, social media optimization, e-mail direct marketing, Display advertising, non-Internet channels such as mobile phones (SMS and MMS), callback, and on-hold mobile ring tones.

Technical outline of browser technologies. Understanding some technical aspects of user visits and engagement data:
<br>— Browser user agents

User visits logging on independent platforms:
<br>— Google analytics
<br>— Matomo (ex Piwik)

Getting your content indexed by search engines :
<br>— SEO optimisation & various search engines
<br>— SEO (search engine optimisation) & SEM (search engine marketing) — how does it work&nbsp;?

Mail campaign tools:
<br>— Pommo mailing
<br>— Mailchimp

Reactive engagement:
<br>— reaction chains for custom actions (example: IFTTT)

Ethical issues of digital marketing:
<br>— data retention
<br>— https://wikimediafoundation.org/news/2019/05/29/lets-talk-about-the-north-face-defacing-wikipedia/

Critical stances in a shifting media landscape:
<br>— dark patterns
<br>— Modes of address
<br>— the Fediverse — alternative independent social networks
<br>— The Great Hack

### Group studies:

Carrying out digital marketing case studies and presenting findings to the group. Example case studies: Seed golf, https://thetoughestjourneygame.com/,

### Class Project:

You will be required to develop a digital marketing communications plan for a small business/charity/club. This will involve working with a client and will be a group project.

## Week 2: 2019_09_26

### marketing communication tools

Gaining a common understanding of Marketing Communication tools available to us today. <a target="blank" href="https://annuel.framapad.org/p/marketing_communications_in_a_digital_era?lang=en">Open this embedded link (in a new tab)</a> and build up the book of definitions with the class.

<iframe src="https://annuel.framapad.org/p/marketing_communications_in_a_digital_era?lang=en"></iframe>

<https://annuel.framapad.org/p/marketing_communications_in_a_digital_era?lang=en>

### group assignment

In groups of three, identify a company, a person, a charity or an organisation of which you will study the digital marketing. You will be asked to make a presentation on your findings to the class group.

Document your findings with screenshots, pdfs, prints, photographs or whatever form you see fit for your presentations.

Your presentation will include:

* an overview of who / what your subject is, the scale of the organisation, their field of operation and geographic location(s)

* an overviewing list of all the digital communications tools & channels your subject employs

* a detailed view of each of the tools & channels you listed in the previous point. Consider giving examples and information about frequency of use. Try to find out if the channels are used individually or simultaneously.

* try to answer the question: how does your subject speak to its target audiences using the communication tools they chose ?

Assignment timeframe: 2 weeks from assignment annoucement. You will be given the two hours of our following course to work with your team, but make sure to make a plan before then as it might take quite some time to gather enough material from each communication channel.

For any questions, mail me at [colm.oneill@itcarlow.ie](mailto:colm.oneill@itcarlow.ie)

# 2019_11_07

## Ethics of online communications,
* <https://www.darkpatterns.org/>
* <https://reallifemag.com/the-mismanaged-heart/>
* <http://tangible.tools/notes-on-the-mismanaged-heart-by-william-davies.html>

# 2019_11_14

![](images/network/network-small.png)

* Matomo (open alternative to Google Analytics)
* OpenGraph (OpenGraph protocol, preformatting social media messages) <https://ogp.me/>

---

# 2019_11_21

Slides for 2019_11_21: <http://tangible.tools/ethics-of-online-communication.html>

## Aral Balkan
a cyborg rights activist, designer, and founder of Small Technology Foundation.

## <https://small-tech.org/>

## <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://video.lqdn.fr/videos/embed/70f2128c-8c06-4cc4-8a5a-bf77e765c8fd" frameborder="0" allowfullscreen></iframe>

## <https://small-tech.slides.com/aral/dear-regulators-dont-throw-the-baby-out-with-the-bathwater#/>

---

## Research and opinion piece (essay) assignment outline:

You are asked to begin researching an opinion piece you will write on the topic of :

**The ethical implications of digital marketing via social media tools.**

Your research and writing will consider ethics, therefor, this is an *individual* project. To fully considering the topic, you must continue to research area by informing yourself on all of the criticised aspects of centralised social media platforms today.

Examples of these issues might be:

* privacy issues
* data retention issues
* platforms selling or trading data points
* ...

### Proposed method:

1. Review the course material introduced over the last three weeks.

2. It is strongly advised to watch 'The great hack' documentary

3. other film pieces include :
> * https://www.thecreepyline.com/
> * Terms And Conditions May Apply (2013)
> * citizenfour (2014)

4. Study grassroots initiatives such as:
> * https://framasoft.org/en/
> * https://fuckoffgoogle.de/
> * https://www.drop-dropbox.com/

5. Next, consider these questions:  
> * What are the implications of using centralised social media platforms as a digital marketing tools ?
> * What are your personal thoughts about traditional centralised social media platforms as a user ? (In regards to privacy ? In regards to platform-trust ? In regards to on-platform advertising ?)
> * What are your personal thoughts about acting for intermediaries (being a marketing message designer) via these mediums ?

Your opinion piece will be **minimum 1200 words, maximum 2000 words**. You will use the Harvard referencing method. There is no set template for this writing assignment, but you are expected to clearly explain your concerns and / or opinions, and back up any claims with quoted research.

## Project rationale

This project aims to develop the learners critical awareness of the online marketing environment. Social media and search engines are highly criticised platforms that also happen to be key tools for the current digital marketing worker. Through this project the learner is expected to question the tools and methods that they might employ to carry out a digital marketing campaign.

Referring back to the Masters in Digital Marketing programme handbook, this assignment will develop 5 out of 6 of the learners gradable attributes:

* Knowledgeable
* Analytical and Creative
* Ethical & Responsible Future Leaders
* Research Driven
* Critical Thinkers  

## the scoring matrix

as a **guideline**, the following items intend to assist you in constructing your opinion piece:

* Relevance to title
* Sound ordering and structuring of material
* Quality and clarity of written word
* Effective use of evidence
* Demonstration of sound understanding of the topic
* Critical evaluation and judgement
* Range of sources used
* Insight and originality
* Referencing

These items will be individually graded: Poor, Weak, Adequate, Good, Very good or Excellent.

The resulting grid will produce your grade.

Project grading weight: 20% of the marks for this subject.  
