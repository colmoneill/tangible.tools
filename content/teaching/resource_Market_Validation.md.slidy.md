Title: Market Validation
Date: 10/10/2019
Template: slidy
Status: hidden
Tags: resource
<div class="slide section level1">

<p>Title: Market Validation Date: 10/10/2019 Template: slidy Status: draft Tags: resource</p>
<!--

pandoc resource_Market_Validation.md --from=Markdown --to=Slidy -o resource_Market_Validation.md.slidy.md

paste to head of new doc:
Title: Market Validation
Date: 10/10/2019
Template: slidy
Status: hidden
Tags: resource

-->
</div>
<div id="section" class="slide section level1">
<h1>10/10/2019</h1>
<ol style="list-style-type: decimal">
<li>a brief refresh on <em>the value proposition</em></li>
<li>outline of the innovation journey</li>
<li>market validation processes</li>
</ol>
</div>
<div id="value-propositon" class="slide section level1">
<h1>Value Propositon</h1>
<p>A value Proposition is a clear statement of the tangible results a customer gets from using your products or services.</p>
<p>Put simply, it is what the customer gets for their money.</p>
</div>
<div id="developing-a-value-proposition" class="slide section level1">
<h1>Developing a value proposition</h1>
<p><b>YOUR Value Proposition:</b><br> [your Business] is for [sector] like [customers] who [problem].<br> We offer them [your solution] which is better than [existing alternatives]<br> because [differentiator]. <br><br> <b>Example Value Proposition: Instant-Opinion.com (called QnA)</b><br> [QnA] is for [conference organisers] like [IP Media] who [need more audience engagement and sponsorship revenue].<br> We offer them [realtime Q&amp;A technology] which is better than [a Twitter wall] because [it is private, moderated and does not require any specialist software].</p>
</div>
<div id="the-innovation-journey" class="slide section level1">
<h1>the innovation journey</h1>
<ol style="list-style-type: decimal">
<li>Design Thinking</li>
<li>Ideation</li>
<li>Entrepreneurial Innovation</li>
<li>Market Validation</li>
<li>Intellectual Property</li>
<li>Commercialisation</li>
<li>New Venture Capital</li>
<li>Risk Management</li>
<li>Initiating Pathways</li>
<li>Clusters &amp; Networks</li>
</ol>
</div>
<div id="market-validation" class="slide section level1">
<h1>Market Validation</h1>
<p>Market validation is a systematic approach to probe, test and validate your market opportunity before you invest significant money in product development. It lets you understand what customers want, allowing you to tailor your product offering to meet their needs and giving you a real competitive edge.</p>
</div>
<div id="market-validation-1" class="slide section level1">
<h1>Market Validation</h1>
<ul>
<li><p>Once you have been through your process of ideation and innovation, and has those few early sales, it’s tempting to think that you can now rest.</p></li>
<li><p>In fact it is quite the opposite. There is a category of people labelled ‘Market Magpies’ are always looking for the next new best thing. The people constitute the easiest sales to make.</p></li>
<li><p>We need to invest more into the product and more into capturing the market to gain more customers to get repeat business and to keep your product profitable.</p></li>
<li><p>Along side all of this, it is important to keep an edge over all your competitors. In order to avoid the Market Chasm, we must discover Market Validation</p></li>
</ul>
</div>
<div id="section-1" class="slide section level1">
<h1><img src="images/entrepreneurial-innovation/market-growth-curve.png" style="width:100%;"></h1>
</div>
<div id="reaching-the-biggest-possible-market" class="slide section level1">
<h1>Reaching the biggest possible market</h1>
<p>Ultimately, you want to want to reach the largest possible market for your product. We previously introduced the idea of the Value Propostion. However, this is based off of what your organisation thinks the customer needs and the products that you can produce. This is what we call a supply lead approach.</p>
</div>
<div id="supply-and-demand" class="slide section level1">
<h1>Supply and demand</h1>
<p>Market validation starts with the supply (your product) and looks at it through the eyes of the demand, your customer.</p>
</div>
<div id="benefits" class="slide section level1">
<h1>Benefits</h1>
<p>Let’s start by identifying the benefits to the customer of your product. We can do this by using the F.A.B. method.</p>
</div>
<div id="f.a.b." class="slide section level1">
<h1>F.A.B.</h1>
<p>Listing and identifying the Features, Attributes and Benefits of your product taps into both sides of our brains, the rational facts based brain and the emotional sides also. We try and think of this both from our point of view as well as from the customers point of view.</p>
</div>
<div id="features-attributes-benefits" class="slide section level1">
<h1>Features, Attributes, Benefits</h1>
<p>Features and Attributes are logical. These are facts about the product. Benefits are emotional. Research has shown that customers buy benefits, not features. So the next step is considering how your products benefits compare to those of your customers.</p>
<p>This ideally requires getting out into the market and asking potential customers what they think of your product.</p>
</div>
<div id="benefits-sell-features-dont" class="slide section level1">
<h1>Benefits sell, features don’t</h1>
<p>A nod to your future communication &amp; marketing campaign:</p>
<ul>
<li><p><em>“People don’t want to buy a quarter-inch drill. They want a quarter-inch hole!”</em></p></li>
<li><p><em>People don’t want to buy web hosting, they want their websites online and accessible to web visitors.</em></p></li>
<li><p><em>People don’t want to buy cooking ebooks, they want to invite their friends and relatives to dinner and have a good time.</em></p></li>
<li><p><em>People don’t want to buy light bulbs. They want to be able to work or read at night.</em></p></li>
</ul>
</div>
<div id="field-market-research" class="slide section level1">
<h1>Field &amp; market research</h1>
<p>Putting products into peoples hands has two benefits; you can ask them to answer specific questions and potential customers will tell you things about your product that others won’t.</p>
</div>
<div id="ways-of-collecting-beta-customer-opinions" class="slide section level1">
<h1>Ways of collecting (beta ?) customer opinions</h1>
<ul>
<li>Market research via surveys</li>
<li>Secret shoppers — artificially placing a product in isles to gauge reactions and collect feedback</li>
<li>Customer feedback</li>
<li>Product reviews</li>
</ul>
<p>These are all valuable ways of getting the benefits finding out what the customer really think about your product</p>
</div>
<div id="ratio-of-income-to-invest-in-market-research" class="slide section level1">
<h1>Ratio of income to invest in market research</h1>
<p>A direct way of ensuring you’re continuously investing in your product is by dedicating a percentage of your product income to market research.</p>
</div>
<div id="upstream-market-research" class="slide section level1">
<h1>Upstream market research</h1>
<p>Doing market research before a product is released is relatively complicated, but there are some inexpesive ways of doing this:</p>
<ul>
<li>Universities and business schools often have staff and students that are able to assist on a project basis.</li>
</ul>
<p>Universities and higher education schools are great places of extensive current and even future market trends. They also have access to large marketing databases build from compilations &amp; interpretations of surveys.</p>
</div>
<div id="upstream-market-research-1" class="slide section level1">
<h1>Upstream market research</h1>
<ul>
<li>Online tools exist to build and carry out surveys. Survey monkey is an example of this. This type of tool is not a clone of Google Forms, they tend to go a lot further in terms of analysis.</li>
</ul>
</div>
<div id="upstream-market-reseach" class="slide section level1">
<h1>Upstream market reseach</h1>
<ul>
<li>If you are confident in your ability to ask the right questions and observe the responses objectively, in house beta customer or test customer cases can be organised. This is common in software and apps, but can be applied to physical products too. Letting someone use a product over a period of time creates good insight.</li>
</ul>
</div>
<div id="the-aim-of-the-market-research" class="slide section level1">
<h1>the aim of the market research</h1>
<p>The goal is to be able to confidently fill out another F.A.B. chart from the customer point of view. Many of these points might overlap with your FAB chart, but having evidence of customers stating a F, a A or a B is key.</p>
<p>Secondly, this should give good indication as to what needs to be <em>added</em>, <em>changed</em> or <em>gotten rid of</em>.</p>
</div>
<div id="market-validation-stage-2-developing-the-minimum-viable-product" class="slide section level1">
<h1>Market validation Stage 2 : Developing the minimum viable product</h1>
<p>this stage aims to gather an understanding of what market shares we could realistically achieve and what the best routes to market may be.</p>
</div>
<div id="minimum-viable-product" class="slide section level1">
<h1>Minimum viable product</h1>
<p>The smallest or simplest thing that you can build or provide that solves enough of the customers pains that they will pay for it.</p>
</div>
<div id="lean-canvas" class="slide section level1">
<h1>Lean Canvas</h1>
<p>A useful and modular tool to develop your business vision and your minimum viable product</p>
</div>
<div id="section-2" class="slide section level1">
<h1><img src="images/entrepreneurial-innovation/lean-canvas.png" style="width:100%;"></h1>
</div>
<div id="boxes" class="slide section level1">
<h1>boxes</h1>
<ul>
<li>box 1: top 3 problems</li>
<li>box 2: customer segments</li>
<li>box 3: unique value proposition</li>
<li>box 4: solutions → use your F.A.B. charts to fill this out</li>
<li>box 5: key metrics, the key activities you need to measure</li>
<li>box 6: revenue streams; What value will out customers really pay for ? How are they paying us today ? How would they like to pay ? What are the main revenue streams ?</li>
<li>box 7: unfair advantage — what is it that can’t be easily copied, or bought that you can provide to the market ?</li>
<li>box 8: channels — the word used to describe how a product reaches the customer</li>
<li>box 9: cost structure — what are the main costs in your business model ? What activities and resources drive these costs ?</li>
</ul>
</div>
<div id="minimum" class="slide section level1">
<h1>Minimum</h1>
<p>You need to build a minimum set of features that enable you to gather feedback from visionary early adopters. But build only what is required ! Later, you can think of additional things that you can add to make the product better and enhance your position in the market.</p>
</div>
<div id="analysing-the-competition" class="slide section level1">
<h1>Analysing the competition</h1>
<ul>
<li>Identify what the two main benefit drivers of your product</li>
<li>These should be the most important aspects of your value proposition</li>
<li>Try to identify where you are in the market and make an educated guess of the size of your market share.</li>
<li>Repeat this for your competitors</li>
</ul>
</div>
<div id="market-validation-at-this-stage" class="slide section level1">
<h1>Market validation at this stage</h1>
<p>At this stage your market validation is all about picking the correct strategy that will help you to differentiate yourself from your competitors.</p>
<ul>
<li>Where is the space in the offering ?</li>
<li>How big is the potential market share ?</li>
<li>You need to get to know your competitors !</li>
</ul>
</div>
<div id="researching-competition" class="slide section level1">
<h1>Researching competition</h1>
<ul>
<li>Find out as much as you can about your competition</li>
<li>List all of your competitors</li>
<li>Discover their prices, their costs, their annual number of sales (if possible)</li>
<li>What do they boast about on their website ?</li>
<li>!! What don’t they talk about ?</li>
</ul>
</div>
<div id="section-3" class="slide section level1">
<h1><img src="images/entrepreneurial-innovation/competition-analysis.png" style="width:100%;"></h1>
</div>
<div id="preparing-routes-to-market" class="slide section level1">
<h1>Preparing routes to market:</h1>
<p>2 key areas:</p>
<ul>
<li>Customer</li>
<li>Consumer</li>
</ul>
</div>
<div id="preparing-routes-to-market-1" class="slide section level1">
<h1>Preparing routes to market:</h1>
<p>2 key areas:</p>
<ul>
<li>Customer : the one who pays for the product</li>
<li>Consumer : the end user</li>
</ul>
<p>ex. child clothes: the parent is the customer, but the child is the consumer</p>
</div>
<div id="routes-to-market" class="slide section level1">
<h1>Routes to market</h1>
<p>Commonly found in three areas:</p>
<ul>
<li>Direct sales to your end user</li>
<li>Intermediaries</li>
<li>Strategic partnerships</li>
</ul>
</div>
<div id="routes-to-market-1" class="slide section level1">
<h1>Routes to market</h1>
<p>Commonly found in three areas:</p>
<ul>
<li><p>Direct sales to your end user: direct to your customer or your end user. This can be done via a sales person, on the road, on the phone, via contract and tenders or by retail, in store or on-line.</p></li>
<li><p>Intermediaries: done by wholesalers or customer service providers. Typically b2b sales (business to business sales), neither of these is the consumer of your product or buying them on behalf of your consumer. They buy your product or service to sell onto others.</p></li>
<li><p>Strategic partnerships: License agreements, embedded in another product or sold along side another product. You sell or agree to sell your product, in, with or as part of another in order to generate sales for both partners</p></li>
</ul>
</div>
<div id="section-4" class="slide section level1">
<h1><img src="images/entrepreneurial-innovation/route-to-market.png" style="width:100%;"></h1>
</div>
<div id="how-to-get-a-market-oriented-product-out-quickly" class="slide section level1">
<h1>How to get a market-oriented product out quickly :</h1>
<ul>
<li>Tightly targeted, narrow markets and know how sales happen for that group</li>
<li>Minimally acceptable feature set with compelling customer benefits (with the correct customer benefits for your target segment, are key to entering the market quickly)</li>
<li>Seeking feedback from your customers and keeping records of customer service interactions (particularly important in continuously improving your product in this early stage)</li>
</ul>
</div>
<div id="market-validation-2" class="slide section level1">
<h1>Market validation</h1>
<p>Market validation doesn’t just happen before sale but during and after also.</p>
</div>
<div id="remember-your-minimum-viable-product" class="slide section level1">
<h1>Remember your minimum viable product</h1>
<ul>
<li>It is not your final product, it allows you to enter the market quickly, start to gain market share, but most of all it gives you the opportunity to gain customer feedback to develop the next product.</li>
</ul>
</div>
