Title: Ethics of online communication
Date: 21/11/2019
Template: slidy
Status: hidden
Tags: resource

<div class="slide section level1">

<p>Title: Ethics of online communication Date: 21/11/2019 Template: slidy Status: draft Tags: resource</p>
<!--

pandoc resource_ethics_online_communication.md --from=Markdown --to=Slidy -o resource_ethics_online_communication.md.slidy.md

paste to head of new doc:
Title: Ethics of online communication
Date: 21/11/2019
Template: slidy
Status: published
Tags: resource

-->
</div>
<div id="weeks-45-46-47-recap" class="slide section level1">
<h1>weeks 45, 46, 47 recap</h1>
</div>
<div id="week-45-2019_11_07" class="slide section level1">
<h1>week 45 — 2019_11_07</h1>
</div>
<div id="social-media-does-not-audit-advertising" class="slide section level1">
<h1>Social media does not audit advertising</h1>
<p>and regulations are coming in</p>
<p><a href="https://www.rte.ie/radio/radioplayer/html5/#/radio1/21651631" class="uri">https://www.rte.ie/radio/radioplayer/html5/#/radio1/21651631</a> <a href="https://www.rte.ie/radio1/morning-ireland/programmes/2019/1107/1089235-morning-ireland-thursday-7-november-2019/" class="uri">https://www.rte.ie/radio1/morning-ireland/programmes/2019/1107/1089235-morning-ireland-thursday-7-november-2019/</a></p>
</div>
<div id="dark-patterns" class="slide section level1">
<h1>Dark Patterns</h1>
<iframe width="560" height="315" src="https://www.youtube.com/embed/kxkrdLI6e6M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
</iframe>
</div>
<div id="the-mismanaged-heart" class="slide section level1">
<h1>The mismanaged heart</h1>
<ul>
<li><a href="https://reallifemag.com/the-mismanaged-heart/" class="uri">https://reallifemag.com/the-mismanaged-heart/</a></li>
<li><a href="http://tangible.tools/notes-on-the-mismanaged-heart-by-william-davies.html" class="uri">http://tangible.tools/notes-on-the-mismanaged-heart-by-william-davies.html</a></li>
</ul>
</div>
<div id="week-46-2019_11_14" class="slide section level1">
<h1>week 46 — 2019_11_14</h1>
</div>
<div id="network-structures" class="slide section level1">
<h1>Network structures</h1>
<p><img src="images/network/network-small.png" /></p>
</div>
<div id="matomo" class="slide section level1">
<h1>Matomo</h1>
<p><img src="https://static.matomo.org/wp-content/uploads/2018/09/matomo-logo-5-colours.jpg" /></p>
</div>
<div id="opengraph-protocol" class="slide section level1">
<h1>OpenGraph protocol</h1>
<p><img src="https://ogp.me/logo.png" style="width:20%;"></p>
<pre><code>&lt;meta property=&quot;og:title&quot; content=&quot;The Rock&quot; /&gt;
&lt;meta property=&quot;og:type&quot; content=&quot;video.movie&quot; /&gt;
&lt;meta property=&quot;og:url&quot; content=&quot;http://www.imdb.com/title/tt0117500/&quot; /&gt;
&lt;meta property=&quot;og:image&quot; content=&quot;http://ia.media-imdb.com/images/rock.jpg&quot; /&gt;</code></pre>
</div>
<div id="week-47-2019_11_21" class="slide section level1">
<h1>week 47 — 2019_11_21</h1>
</div>
<div id="a-reintroduction-to-a-key-communication-theory-statement" class="slide section level1">
<h1>a reintroduction to a key communication theory statement:</h1>
</div>
<div id="the-medium-is-the-message" class="slide section level1">
<h1>The medium is the message</h1>
</div>
<div id="section" class="slide section level1">
<h1><iframe width="560" height="315" src="https://www.youtube.com/embed/Ko6J9v1C9zE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></h1>
</div>
<div id="section-1" class="slide section level1">
<h1><iframe width="560" height="315" src="https://www.youtube.com/embed/gCr2binb4Fs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></h1>
</div>
<div id="from-wikipedia" class="slide section level1">
<h1>from Wikipedia</h1>
<blockquote>
<p>“The medium is the message” is a phrase coined by Marshall McLuhan introduced in McLuhan’s book Understanding Media: The Extensions of Man, published in 1964.[1] McLuhan proposes that a medium itself, not the content it carries, should be the focus of study. He said that a medium affects the society in which it plays a role not only by the content delivered over the medium, but also by the characteristics of the medium itself.</p>
</blockquote>
</div>
<div id="in-todays-media-environment" class="slide section level1">
<h1>In today’s media environment:</h1>
<p><strong>Centralised</strong> social media platforms are being highly criticised.</p>
<p><img src="http://networkcultures.org/unlikeus/wp-content/uploads/sites/2/2013/03/networktypes.png" /></p>
</div>
<div id="the-statement-if-you-are-not-paying-for-it-you-are-the-product-has-never-rung-truer-since-christopher-wylies-whistle-blowing." class="slide section level1">
<h1>The statement <em>if you are not paying for it; you are the product</em> has never rung truer since Christopher Wylie’s whistle-blowing.</h1>
<p><img src="https://s.aolcdn.com/hss/storage/midas/7e366766c8b14fc12d640cc89e5c188c/206379980/ca-ed.jpg" /></p>
</div>
<div id="section-2" class="slide section level1">
<h1><iframe width="560" height="315" src="https://www.youtube.com/embed/iX8GxLP1FHo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></h1>
</div>
<div id="the-social-media-landscape-is-rapidly-changing.-as-actors-and-intermediaries-on-these-networks-we-need-to-understand-what-the-ethical-implications-of-our-actions-and-presence-on-these-networks-mean." class="slide section level1">
<h1>The social media landscape is rapidly changing. As actors and intermediaries on these networks we need to understand what the ethical implications of our actions and presence on these networks mean.</h1>
</div>
<div id="aral-balkan" class="slide section level1">
<h1>Aral Balkan</h1>
<p>a cyborg rights activist, designer, and founder of Small Technology Foundation.</p>
</div>
<div id="httpssmall-tech.org" class="slide section level1">
<h1><a href="https://small-tech.org/" class="uri">https://small-tech.org/</a></h1>
</div>
<div id="httpssmall-tech.slides.comaraldear-regulators-dont-throw-the-baby-out-with-the-bathwater" class="slide section level1">
<h1><a href="https://small-tech.slides.com/aral/dear-regulators-dont-throw-the-baby-out-with-the-bathwater#/" class="uri">https://small-tech.slides.com/aral/dear-regulators-dont-throw-the-baby-out-with-the-bathwater#/</a></h1>
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://video.lqdn.fr/videos/embed/70f2128c-8c06-4cc4-8a5a-bf77e765c8fd" frameborder="0" allowfullscreen>
</iframe>
</div>
