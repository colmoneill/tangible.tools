Title: Colour theory
Date: 20/10/2019
Template: slidy
Status: draft
Tags: resource

<!--

pandoc resource_colour_theory.md --from=Markdown --to=Slidy -o resource_colour_theory.md.slidy.md

paste to head of new doc:
Title: Colour theory
Date: 20/10/2019
Template: slidy
Status: published
Tags: resource

-->

# Colour theory

# initial question:

What is colour ?

# Colour is light

Light is a electromagnetic radiation, meaning, on it's own it is invisible, we can use tools to measure it, but to our eyes, light is invisible. We see the effects of light.

# Colour is a fragment of daylight

<img src="images/colour-theory/Light_dispersion_conceptual_waves.gif" style="width:100%;">

# <img src="images/colour-theory/prism.jpg" style="width:100%;">

# <img src="images/colour-theory/wavelength_graph.jpg" style="width:100%;">

<img src="https://upload.wikimedia.org/wikipedia/commons/c/c4/Rendered_Spectrum.png" style="width:100%;">

# Colour is our eyes reading all of the wavelengths that are present in light.

Our eyes are full of various receptors that enable us to see colour

<img src="images/colour-theory/human-eye-cells.jpg" style="width:100%;">

# Rods & cones

Both of these are what we call photoreceptor cells, meaning, sensitive to light.

* Rods are most numerous, about 120 million of them per eye, they are more *sensitive* to light than cones, but not sensitive to colours. They give us the ability to see in low light.

* Cones are counted in the 6 million per eye, they are the cells that let us see various colours in the day time. They are sensitive to different wavelengths.

# We only see the effects of light.

What we see with our cognitive brain is light hitting a surface.

This is what we call *additive theory*. It can also be called *light theory*, it deals with radiated and filtered light.

# Primary colours in additive light theory:

* Red
* Green
* Blue

# There is a second colour theory

What happens if you mix Red, Green and Blue paint together ?

# The second theory deals with how light is absorbed

it is called *subtractive theory* and can also be called *pigment theory* because we are dealing with pigments of colour, absorbing certain wavelengths of the light spectrum

<img src="images/colour-theory/additive-and-subtractive.jpg" style="width:100%;">

# <img src="images/colour-theory/colorbasics001.gif" style="width:100%;">

# <img src="images/colour-theory/CMYK-color-combinations.png" style="width:100%;">

# Additive vs Subtractive

* Additive colour theory deals with how we see light and is used on screens, in video and photography

* Subtractive colour theory deals with how light is reflected off of pages, canvases or card and is used in printing and painting.

# Screens use RGB

<img src="images/colour-theory/lcd-pixel-lg.png" style="width:100%;">

# <img src="images/colour-theory/lcd-pixel.jpg" style="width:100%;">

# <img src="images/colour-theory/macro-screen-lcd.jpg" style="width:100%;">

# <img src="images/colour-theory/lcd-type.jpg" style="width:100%;">

# In graphics practice, we need to be able to codify colours

We need colour codes to be able to communicate colours between people and software but also between designers and printers

There are two basic colour systems to reflect both the additive and subtractive

* RGB for screen use, web use, video and photography use

but as soon as you plan on printing your documents on screen, you must switch to

* CMYK for inks and paints.

# RGB & CMYK

* RBG = Red, Blue, Green

* CMYK = Cyan, Magenta, Yellow, K: for Black

(K is for Key, used instead of B for Black, because in some printing cases, we use a 4th or even 5th colour instead of Black for Key reference)

# general terms to describe colours

* Hue
* Saturation / chroma
* Value / Luminosity

# Hue:

The property of colors by which they can be perceived as ranging from red through yellow, green, and blue, as determined by the dominant wavelength of the light.

<img src="images/colour-theory/hue1.jpg" style="width:100%;">

# <img src="images/colour-theory/hue2.jpg" style="width:100%;">

# <img src="images/colour-theory/hue3.jpg" style="width:100%;">

# Saturation / chroma:

The vividness of a color's hue. Saturation measures the degree to which a color differs from a gray of the same darkness or lightness.

<img src="images/colour-theory/saturation1.jpg" style="width:100%;">

<img src="images/colour-theory/saturation2.jpg" style="width:100%;">

<img src="images/colour-theory/saturation3.jpg" style="width:100%;">

# Value / luminosity

Attribute of visual sensation according to which an area appears to emit more or less light. Lightness, more or less white in the color composition.

<img src="images/colour-theory/value1.jpg" style="width:100%;">

<img src="images/colour-theory/value2.jpg" style="width:100%;">

<img src="images/colour-theory/value3.jpg" style="width:100%;">


# Setting colours in software

<img src="images/colour-theory/color_picker2.png" style="width:100%;">

<img src="images/colour-theory/color_picker3.png" style="width:100%;">


# the colour wheel

<img src="images/colour-theory/Color-Wheel-20.gif" style="width:100%;">

# colour formulas

* Complementary colours
* Analogous colours
* Triadic colours

# Complementary

<img src="images/colour-theory/Complementary-3-column.png" style="width:100%;">

# Analogous

<img src="images/colour-theory/Analogous-3-column.png" style="width:100%;">

# Triadic

<img src="images/colour-theory/Triadic-3-column.png" style="width:100%;">

# Other key notions to describe colour:

<img src="images/colour-theory/Warm-colors-2-column.png" style="width:100%;">

<img src="images/colour-theory/Cool-colors-2-column.png" style="width:100%;">

<img src="images/colour-theory/Monochromatic-2-column.png" style="width:100%;">

<img src="images/colour-theory/Grayscale-2-column.png" style="width:100%;">


# useful tools

<https://color.adobe.com/create/color-wheel>
<br><https://colorpalettes.net/>
<br><https://www.color-hex.com/color-palettes/>
<br><https://colorhunt.co/>

# recap

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/_2LLXnUdUIc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
