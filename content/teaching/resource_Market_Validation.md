Title: Market Validation
Date: 10/10/2019
Template: slidy
Status: draft
Tags: resource

<!--

pandoc resource_Market_Validation.md --from=Markdown --to=Slidy -o resource_Market_Validation.md.slidy.md

paste to head of new doc:
Title: Market Validation
Date: 10/10/2019
Template: slidy
Status: hidden
Tags: resource

-->

# 10/10/2019

1. a brief refresh on *the value proposition*
2. outline of the innovation journey
3. market validation processes

# Value Propositon
A value Proposition is a clear statement of the tangible results a customer gets from using your products or services.

Put simply, it is what the customer gets for their money.

# Developing a value proposition

<b>YOUR Value Proposition:</b><br>
[your Business] is for [sector] like [customers] who [problem].<br>
We offer them [your solution] which is better than [existing alternatives]<br>
because [differentiator].
<br><br>
<b>Example Value Proposition: Instant-Opinion.com (called QnA)</b><br>
[QnA] is for [conference organisers] like [IP Media] who [need more audience engagement and sponsorship revenue].<br>
We offer them [realtime Q&A technology] which is better than [a Twitter wall] because [it is private, moderated and does not require any specialist software].


# the innovation journey

1. Design Thinking
2. Ideation
3. Entrepreneurial Innovation
4. Market Validation
5. Intellectual Property
6. Commercialisation
7. New Venture Capital
8. Risk Management
9. Initiating Pathways
10. Clusters & Networks

# Market Validation

Market validation is a systematic approach to probe, test and validate your market opportunity before you invest significant money in product development. It lets you understand what customers want, allowing you to tailor your product offering to meet their needs and giving you a real competitive edge.

# Market Validation

* Once you have been through your process of ideation and innovation, and has those few early sales, it's tempting to think that you can now rest.

* In fact it is quite the opposite. There is a category of people labelled 'Market Magpies' are always looking for the next new best thing. The people constitute the easiest sales to make.

* We need to invest more into the product and more into capturing the market to gain more customers to get repeat business and to keep your product profitable.

* Along side all of this, it is important to keep an edge over all your competitors. In order to avoid the Market Chasm, we must discover Market Validation

# <img src="images/entrepreneurial-innovation/market-growth-curve.png" style="width:100%;">

# Reaching the biggest possible market

Ultimately, you want to want to reach the largest possible market for your product. We previously introduced the idea of the Value Propostion. However, this is based off of what your organisation thinks the customer needs and the products that you can produce. This is what we call a supply lead approach.

# Supply and demand

Market validation starts with the supply (your product) and looks at it through the eyes of the demand, your customer.

# Benefits

Let's start by identifying the benefits to the customer of your product. We can do this by using the F.A.B. method.

# F.A.B.

Listing and identifying the Features, Attributes and Benefits of your product taps into both sides of our brains, the rational facts based brain and the emotional sides also. We try and think of this both from our point of view as well as from the customers point of view.

# Features, Attributes, Benefits

Features and Attributes are logical. These are facts about the product. Benefits are emotional. Research has shown that customers buy benefits, not features. So the next step is considering how your products benefits compare to those of your customers.

This ideally requires getting out into the market and asking potential customers what they think of your product.

# Benefits sell, features don't

A nod to your future communication & marketing campaign:

* *“People don’t want to buy a quarter-inch drill. They want a quarter-inch hole!”*

* *People don’t want to buy web hosting, they want their websites online and accessible to web visitors.*

* *People don’t want to buy cooking ebooks, they want to invite their friends and relatives to dinner and have a good time.*

* *People don’t want to buy light bulbs. They want to be able to work or read at night.*

# Field & market research

Putting products into peoples hands has two benefits; you can ask them to answer specific questions and potential customers will tell you things about your product that others won't.

# Ways of collecting (beta ?) customer opinions

* Market research via surveys
* Secret shoppers — artificially placing a product in isles to gauge reactions and collect feedback
* Customer feedback
* Product reviews

These are all valuable ways of getting the benefits finding out what the customer really think about your product

# Ratio of income to invest in market research

A direct way of ensuring you're continuously investing in your product is by dedicating a percentage of your product income to market research.

# Upstream market research

Doing market research before a product is released is relatively complicated, but there are some inexpesive ways of doing this:

* Universities and business schools often have staff and students that are able to assist on a project basis.

Universities and higher education schools are great places of extensive current and even future market trends. They also have access to large marketing databases build from compilations & interpretations of surveys.

# Upstream market research

* Online tools exist to build and carry out surveys. Survey monkey is an example of this. This type of tool is not a clone of Google Forms, they tend to go a lot further in terms of analysis.

# Upstream market reseach

* If you are confident in your ability to ask the right questions and observe the responses objectively, in house beta customer or test customer cases can be organised. This is common in software and apps, but can be applied to physical products too. Letting someone use a product over a period of time creates good insight.

# the aim of the market research

The goal is to be able to confidently fill out another F.A.B. chart from the customer point of view. Many of these points might overlap with your FAB chart, but having evidence of customers stating a F, a A or a B is key.

Secondly, this should give good indication as to what needs to be *added*, *changed* or *gotten rid of*.

# Market validation Stage 2 : Developing the minimum viable product

this stage aims to gather an understanding of what market shares we could realistically achieve and what the best routes to market may be.

# Minimum viable product

The smallest or simplest thing that you can build or provide that solves enough of the customers pains that they will pay for it.

# Lean Canvas

A useful and modular tool to develop your business vision and your minimum viable product

# <img src="images/entrepreneurial-innovation/lean-canvas.png" style="width:100%;">

# boxes

* box 1: top 3 problems
* box 2: customer segments
* box 3: unique value proposition
* box 4: solutions → use your F.A.B. charts to fill this out
* box 5: key metrics, the key activities you need to measure
* box 6: revenue streams; What value will out customers really pay for ? How are they paying us today ? How would they like to pay ? What are the main revenue streams ?
* box 7: unfair advantage — what is it that can't be easily copied, or bought that you can provide to the market ?
* box 8: channels — the word used to describe how a product reaches the customer
* box 9: cost structure — what are the main costs in your business model ? What activities and resources drive these costs ?

# Minimum

You need to build a minimum set of features that enable you to gather feedback from visionary early adopters. But build only what is required ! Later, you can think of additional things that you can add to make the product better and enhance your position in the market.

# Analysing the competition

* Identify what the two main benefit drivers of your product
* These should be the most important aspects of your value proposition
* Try to identify where you are in the market and make an educated guess of the size of your market share.
* Repeat this for your competitors

# Market validation at this stage

At this stage your market validation is all about picking the correct strategy that will help you to differentiate yourself from your competitors.

* Where is the space in the offering ?
* How big is the potential market share ?
* You need to get to know your competitors !

# Researching competition

* Find out as much as you can about your competition
* List all of your competitors
* Discover their prices, their costs, their annual number of sales (if possible)
* What do they boast about on their website ?
* !! What don't they talk about ?

# <img src="images/entrepreneurial-innovation/competition-analysis.png" style="width:100%;">

# Preparing routes to market:

2 key areas:

* Customer
* Consumer

# Preparing routes to market:

2 key areas:

* Customer : the one who pays for the product
* Consumer : the end user

ex. child clothes: the parent is the customer, but the child is the consumer

# Routes to market

Commonly found in three areas:

* Direct sales to your end user
* Intermediaries
* Strategic partnerships

# Routes to market

Commonly found in three areas:

* Direct sales to your end user: direct to your customer or your end user. This can be done via a sales person, on the road, on the phone, via contract and tenders or by retail, in store or on-line.

* Intermediaries: done by wholesalers or customer service providers. Typically b2b sales (business to business sales), neither of these is the consumer of your product or buying them on behalf of your consumer. They buy your product or service to sell onto others.

* Strategic partnerships: License agreements, embedded in another product or sold along side another product. You sell or agree to sell your product, in, with or as part of another in order to generate sales for both partners

# <img src="images/entrepreneurial-innovation/route-to-market.png" style="width:100%;">

# How to get a market-oriented product out quickly :

* Tightly targeted, narrow markets and know how sales happen for that group
* Minimally acceptable feature set with compelling customer benefits (with the correct customer benefits for your target segment, are key to entering the market quickly)
* Seeking feedback from your customers and keeping records of customer service interactions (particularly important in continuously improving your product in this early stage)

# Market validation

Market validation doesn't just happen before sale but during and after also.

# Remember your minimum viable product

* It is not your final product, it allows you to enter the market quickly, start to gain market share, but most of all it gives you the opportunity to gain customer feedback to develop the next product.
