Title: Ethics of online communication
Date: 21/11/2019
Template: slidy
Status: draft
Tags: resource

<!--

pandoc resource_ethics_online_communication.md --from=Markdown --to=Slidy -o resource_ethics_online_communication.md.slidy.md

paste to head of new doc:
Title: Ethics of online communication
Date: 21/11/2019
Template: slidy
Status: published
Tags: resource

-->

# weeks 45, 46, 47 recap

# week 45 — 2019_11_07

# Social media does not audit advertising

and regulations are coming in

<https://www.rte.ie/radio/radioplayer/html5/#/radio1/21651631>
<https://www.rte.ie/radio1/morning-ireland/programmes/2019/1107/1089235-morning-ireland-thursday-7-november-2019/>

# Dark Patterns

<iframe width="560" height="315" src="https://www.youtube.com/embed/kxkrdLI6e6M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# The mismanaged heart

* <https://reallifemag.com/the-mismanaged-heart/>
* <http://tangible.tools/notes-on-the-mismanaged-heart-by-william-davies.html>

# week 46 — 2019_11_14

# Network structures

![](images/network/network-small.png)

# Matomo

![](https://static.matomo.org/wp-content/uploads/2018/09/matomo-logo-5-colours.jpg)

# OpenGraph protocol

<img src="https://ogp.me/logo.png" style="width:20%;">

```
<meta property="og:title" content="The Rock" />
<meta property="og:type" content="video.movie" />
<meta property="og:url" content="http://www.imdb.com/title/tt0117500/" />
<meta property="og:image" content="http://ia.media-imdb.com/images/rock.jpg" />
```

# week 47 — 2019_11_21

# a reintroduction to a key communication theory statement:

# The medium is the message

# <iframe width="560" height="315" src="https://www.youtube.com/embed/Ko6J9v1C9zE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# <iframe width="560" height="315" src="https://www.youtube.com/embed/gCr2binb4Fs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# from Wikipedia

> "The medium is the message" is a phrase coined by Marshall McLuhan introduced in McLuhan's book Understanding Media: The Extensions of Man, published in 1964.[1] McLuhan proposes that a medium itself, not the content it carries, should be the focus of study. He said that a medium affects the society in which it plays a role not only by the content delivered over the medium, but also by the characteristics of the medium itself.

# In today's media environment:

**Centralised** social media platforms are being highly criticised.

![](http://networkcultures.org/unlikeus/wp-content/uploads/sites/2/2013/03/networktypes.png)

# The statement *if you are not paying for it; you are the product* has never rung truer since Christopher Wylie's whistle-blowing.

![](https://s.aolcdn.com/hss/storage/midas/7e366766c8b14fc12d640cc89e5c188c/206379980/ca-ed.jpg)

# <iframe width="560" height="315" src="https://www.youtube.com/embed/iX8GxLP1FHo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# The social media landscape is rapidly changing. As actors and intermediaries on these networks we need to understand what the ethical implications of our actions and presence on these networks mean.

# Aral Balkan
a cyborg rights activist, designer, and founder of Small Technology Foundation.

# <https://small-tech.org/>

# <https://small-tech.slides.com/aral/dear-regulators-dont-throw-the-baby-out-with-the-bathwater#/>

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://video.lqdn.fr/videos/embed/70f2128c-8c06-4cc4-8a5a-bf77e765c8fd" frameborder="0" allowfullscreen></iframe>
