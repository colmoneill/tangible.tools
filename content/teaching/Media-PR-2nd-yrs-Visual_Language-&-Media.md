Date: 2019/09/02
Title: Visual Language and Media

#### BA Media and PR

## Course schedule:
* Wednesdays from 10.00 → 11.00 (room A100) — Lecture
* Wednesdays from 14.00 → 16.00 (room A310) — Practical

### Course introduction 2019_09_11

Brief subjects outline:

* Photography workflow: from camera to print

→ How to shoot RAW format photographs, import them into Camera Raw (a Photoshop tool), work on our photographs in Photoshop, then export them for other uses.

* Composition basics:

→ Making images, balancing the objects and elements in an image, choosing scale, choosing colours, choosing graphical or photographs, hand drawings, and adjusting them to one another on a page, poster, logo, flyer etc.

* Typography: selecting and using fonts

→ Typography is the art and technique of arranging type to make written language legible, readable, and appealing when displayed. The arrangement of type involves selecting typefaces, point sizes, line lengths, line-spacing (leading), and letter-spacing (tracking), and adjusting the space between pairs of letters (kerning).

* Vectors VS Bitmap: digital images

→ What is a vector image, what is a bitmap image, and how can we use them

* Page design — editorial design:

→ How to design good books, booklets, pages, flyers, leaflets, how to make imposed booklets (allowing one to fold printed pages into a booklet), how to prepare files for self-printing and professional printing.

### 2019/10/16

### Film poster assignment

Visual communications gives us the power to convey an idea, or reference an item with very few symbols. This assignment aims to stimulate the message creation part of visual communication and the practical aspect of technically building the message using appropriate software.

Select three movies that you know well. A requirement for this assignment is that the three films you have picked are from different genres of cinema. For example, you could select one thriller, one documentary and one super hero film. Selecting three films from the same genre will make the assignment harder for you, so make sure each film are different and that you know them well.

From each film, identify key elements of the narrative, key scenes and key elements of dialog. Think about the tipping points or resolving elements of the films. Make a list of these and try to find how you could summarise the entire film using only a few symbols. These symbols might be important objects, significant props, elements of costumes, unique pieces of a characters physique, etc.

Also think of the color scheme used throughout the film. For example, a horror film is often very dark with very saturated accent colors. Other example: a sport film will have a lot of daytime colors like skies and bright jersey colors. Use these key colors in the poster you will design.

Use the following examples to guide how to think about your films and how you will translate these stories into a poster.

Your posters must include:
* the full titles of the films
* the name of the director of the film
* the visual construct you have chosen to sum up the film with
* a maximum of 5 different colors

![](images/minimal-poster/adc1.png)

![](images/minimal-poster/adc2.png)

![](images/minimal-poster/adc3.png)

![](images/minimal-poster/adc4.png)

![](images/minimal-poster/adc5.png)

![](images/minimal-poster/adc6.png)

![](images/minimal-poster/CastawayPrint.jpg)

![](images/minimal-poster/InceptionLarge.jpeg)

![](images/minimal-poster/matrix.jpg)

![](images/minimal-poster/MoonriseKingdomPrint.jpg)

![](images/minimal-poster/TheItalianJobPrint.jpg)

![](images/minimal-poster/WhiplashPrint.jpeg)

![](images/minimal-poster/ZackAndMiri.jpg)

# outline of a possible design process
* work on paper first
* write out ideas, list more than necessary, letting your brain work on more than the first ideas will give deeper thoughts and better ideas
* annotate your ideas, detail what it is your are talking about, identify key words, list key words and look for synonyms, sometimes other vocabulary helps us think about what we are designing
* draw early and draw often: making tiny post-stamp sized sketches, maquettes, outlines or wireframes of what your designs could look like will help you accomplish your ideas faster and more efficiently
* list out the technical difficulties and see if you can solve them or go around them
* lastly open your software tool of choice

# illustrator resources
* pen tool exercise
* composition basics
* [color theory basics](/colour-theory.html)
* pathfinder tool
