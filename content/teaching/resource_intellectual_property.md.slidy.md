Title: Intellectual property
Date: 17/10/2019
Template: slidy
Status: hidden
Tags: resource

<div class="slide section level1">

<p>Title: Intellectual property Date: 17/10/2019 Template: slidy Status: draft Tags: resource</p>
<!--

pandoc resource_intellectual_property.md --from=Markdown --to=Slidy -o resource_intellectual_property.md.slidy.md

paste to head of new doc:
Title: Intellectual property
Date: 17/10/2019
Template: slidy
Status: published
Tags: resource

-->
</div>
<div id="section" class="slide section level1">
<h1>17/10/2019</h1>
<!-- # <img src="images/entrepreneurial-innovation/route-to-market.png" style="width:100%;"> -->
</div>
<div id="intellectual-property" class="slide section level1">
<h1>0. Intellectual property</h1>
<p>the manifestation of ideas, creativity and invention in a tangible form</p>
</div>
<div id="it-is-called-property" class="slide section level1">
<h1>it is called property</h1>
<p>Intellectual property is often discuss in the same terms as commercial or physical property because it can be considered an asset to your organisation"</p>
</div>
<div id="the-will-to-enforce" class="slide section level1">
<h1>0.1 The will to enforce</h1>
<p>even if you already have a patent, trade mark or copyright, you must have the will and the resources to enforce the protection of these by taking somebody who is infringing one of these to the civil courts</p>
<ul>
<li>This can be very expensive</li>
<li>Time consuming</li>
</ul>
<p>↑ particularly if the infringement happens in another country</p>
</div>
<div id="two-types-of-intellectual-property" class="slide section level1">
<h1>0.2 two types of intellectual property</h1>
<ul>
<li>registered</li>
<li>unregistered</li>
</ul>
</div>
<div id="section-1" class="slide section level1">
<h1><img src="images/entrepreneurial-innovation/ip-types.png" style="width:100%;"></h1>
</div>
<div id="sources" class="slide section level1">
<h1>0.3 sources:</h1>
<ul>
<li><a href="https://www.patentsoffice.ie/en/types-of-ip/" class="uri">https://www.patentsoffice.ie/en/types-of-ip/</a></li>
<li><a href="http://www.bucanier.eu/home/" class="uri">http://www.bucanier.eu/home/</a></li>
</ul>
</div>
<div id="types-of-registered-intellectual-property" class="slide section level1">
<h1>1. types of registered intellectual property</h1>
<ul>
<li>patents</li>
<li>registered designs</li>
<li>registered trademarks</li>
<li>plant varieties</li>
</ul>
</div>
<div id="patents" class="slide section level1">
<h1>1.1 patents</h1>
<p>Gives its holder, for a limited amount of time, the right to stop other from exploiting the invention. There are renewal fees to be paid to keep the patent operative.</p>
</div>
<div id="what-can-be-patented" class="slide section level1">
<h1>1.1 what can be patented</h1>
<p>Patent protection can be applied to a wide range of inventions such as appliances and mechanical devices, biological and chemical inventions and computer related inventions.</p>
</div>
<div id="what-can-not-be-patented" class="slide section level1">
<h1>1.1 what can not be patented</h1>
<p>Not all inventions qualify for the grant of a patent. The Irish Patents Act specifically excludes the following subjects from patentability:</p>
<ol style="list-style-type: lower-roman">
<li><p>Discoveries and aesthetic creations:</p>
<ul>
<li>A discovery, a scientific theory or a mathematical method;</li>
<li>An aesthetic creation;</li>
<li>A scheme, rule or method for performing a mental act, playing a game or doing business, or a computer program; or</li>
<li>The presentation of information.</li>
</ul></li>
</ol>
<p>Although such subject matter or activities are not patentable, their use or application may be patentable.</p>
<p>For example, a scheme or method for playing a game is not patentable, but it is possible to obtain patent protection for a novel apparatus for playing a game.</p>
<p>Also, the exclusion from patentability of computer programs does not prevent the granting of patents for inventions involving the use of such programs, as long as a technical effect is achieved by its implementation.</p>
</div>
<div id="software" class="slide section level1">
<h1>1.1 Software</h1>
<p>While it is not possible to obtain a patent on software per se, patents may be granted for inventions requiring the use of software to achieve their purpose. This, however, is conditional on the software having a “technical effect” when the programme is run. Such effect may, for example, be found in the control of an industrial process or in the internal functioning of the computer itself.</p>
</div>
<div id="patent-geography" class="slide section level1">
<h1>1.1 patent geography</h1>
<p>Patents are territorial and give an exclusive right in the country where the patent has been granted as long as the patent is renewed each year through the payment of a renewal fee, e.g. an Irish patent is only valid in Ireland (Republic of Ireland only).</p>
<p>If you are seeking protection in other countries outside of Ireland, you can apply for a patent in that country. Alternatively, you can apply to the European Patent Office or use the Patent Co-operation Treaty system.</p>
</div>
<div id="registered-trade-marks" class="slide section level1">
<h1>1.2 registered trade marks</h1>
<p>Trade marks are any signs that can be represented graphically and are capable of distinguishing the goods or services of one business from those of another.</p>
</div>
<div id="function-of-tm" class="slide section level1">
<h1>1.2 function of TM</h1>
<p>The essential function of a trademark is to exclusively identify the commercial source or origin of products or services, so a trademark, properly called, indicates source or serves as a badge of origin. In other words, trademarks serve to identify a particular business as the source of goods or services. The use of a trademark in this way is known as trademark use. Certain exclusive rights attach to a registered mark.</p>
</div>
<div id="famous-trade-mark" class="slide section level1">
<h1>1.2 famous trade mark</h1>
<p><img src="http://stmedia.startribune.com/images/ows_152598280462492.jpg" style="width:100%;"></p>
</div>
<div id="graphical-signs-of-tm" class="slide section level1">
<h1>1.2 graphical signs of TM</h1>
<p>Registered trademarks can use the R symbol, whereas unregistered trademarks tend to use the TM symbol. The TM symbol could be described as a visual explanation of what a logo is.</p>
<p>in Unicode :</p>
<ul>
<li>U+2122 ™ TRADE MARK SIGN.</li>
<li>U+24C7 Ⓡ CIRCLED LATIN CAPITAL LETTER R</li>
</ul>
</div>
<div id="registered-designs" class="slide section level1">
<h1>1.3 registered designs</h1>
<p>“Design” means the appearance of some or all of a product resulting from the features such as the lines, contours, colour, shape, texture or materials of the product itself or its ornamentation.</p>
</div>
<div id="what-is-registrable-under-a-registered-design" class="slide section level1">
<h1>1.3 What is Registrable under a registered design ?</h1>
<p><br> In order to be registrable a design must be new and have individual character:</p>
<h2 id="new">New</h2>
<p>A design is considered to be new if no identical design has been made available to the public before the date of filing of the application for registration or where priority is claimed, the date of priority.</p>
<h2 id="individual-character">Individual character</h2>
<p>The requirement to have “individual character” is met if the overall impression produced by a design on an informed user differs from the overall impression produced on such a user by any earlier design which has been made available to the public.</p>
<p>Traditionally, protectable designs relate to manufactured products such as the shape of a shoe, the design of an earring or the ornamentation on a teapot. In the digital world, however, protection is gradually extending in some countries to a number of other products and types of design. These include electronic desktop icons generated by computer codes, typefaces, the graphic display on computer monitors and mobile telephones, among other things.</p>
</div>
<div id="what-cannot-be-registered" class="slide section level1">
<h1>1.3 What cannot be registered?</h1>
<p>Designs that relate to how a product functions or for parts that in normal use are not visible, or designs that are contrary to public policy or to accepted principles of morality or which constitute an infringement of a copyright work may not be registered. See The Industrial Designs Act, 2001 for further details.</p>
</div>
<div id="types-of-un-registered-ip" class="slide section level1">
<h1>2. types of un-registered ip</h1>
<ul>
<li>copyright</li>
<li>common law rights</li>
<li>design right</li>
<li>trade secrets (including know-how)</li>
</ul>
</div>
<div id="section-2" class="slide section level1">
<h1><img src="images/entrepreneurial-innovation/author-schema.svg" style="width:70%;"></h1>
</div>
<div id="copyright-defining-copyright" class="slide section level1">
<h1>2.1 copyright — defining copyright</h1>
<p>“As humans, we share the realm of abstract ideas. What artists do, is to take an abstract idea and make a series of creative choices in formed by their personality in order to create an expression that is the artistic work.”</p>
</div>
<div id="copyright-defining-copyright-1" class="slide section level1">
<h1>2.1 copyright — defining copyright</h1>
<p>“Because your personality is unique, your artistic work is original. And because the work is so tightly tied to your personality, you should be allowed to have some control over it.</p>
<p>And because copyright was invented by writers and publishers, not by painters, the control that you have is not over the physical object through which your expression is fixed, but over its copies.”</p>
</div>
<div id="copyright-regulating-copies-and-the-reuse-of-your-work" class="slide section level1">
<h1>2.1 copyright — regulating copies and the reuse of your work</h1>
<p>Next to regulating the copies of a work, copyright also regulates the reuse of your copyrighted work in a new work by another author. Most forms of sampling, adaptation and appropriation are, as far as copyright is concerned, still copies.</p>
<p>The expected benefit is double-edged.</p>
<ul>
<li><p>it is economic —you can profit from attention to your work by selling, for example, the film rights to a text you wrote or the reproduction rights to an image you created.</p></li>
<li><p>On the other hand, the benefit is artistic control —you can limit who gets to reuse the work and to what extent, in which case your rights limit those of other artists.</p></li>
</ul>
</div>
<div id="copyright-what-gets-to-be-copyrighted" class="slide section level1">
<h1>2.1 copyright — What gets to be copyrighted ?</h1>
<p>the Berne Convention, uses the term “literary and artistic works.” It defines this as “every production in the literary, scientific and artistic domain, whatever may be the mode or form of its expression.”</p>
</div>
<div id="copyright-what-gets-to-be-copyrighted-1" class="slide section level1">
<h1>2.1 copyright — What gets to be copyrighted ?</h1>
<p>The expression “production in the literary, scientific and artistic domain” may seem to restrict the scope of copyright to the context of professional creative production, but that’s not really the case. Judges have been reticent to arbitrate what works be long to the artistic domain because they would then have to determine who is an artist and who is not.</p>
<p>A more open definition has come to be employed. Judges ask: Does the expression in question show the imprint of a personality?</p>
</div>
<div id="copyright-what-gets-to-be-copyrighted-2" class="slide section level1">
<h1>2.1 copyright — What gets to be copyrighted ?</h1>
<p>And because that sounds vague, here’s an other way to phrase it:</p>
<p>Is the expression the result of a number of free and creative choices by a human being (yes, copyright is speciesist)? You don’t have to consider your self an artist, be validated as such, or even consider what you do as art to get copyright. Your holiday snap shots and the drawings made in creative therapy all have copyright, as do everyone else’s.</p>
</div>
<div id="section-3" class="slide section level1">
<h1><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Macaca_nigra_self-portrait_large.jpg/800px-Macaca_nigra_self-portrait_large.jpg"></h1>
</div>
<div id="section-4" class="slide section level1">
<h1><img src="https://upload.wikimedia.org/wikipedia/commons/3/30/Macaca_nigra_self-portrait_full_body.jpg"></h1>
</div>
<div id="copyright-ideas-expression" class="slide section level1">
<h1>2.1 copyright — ideas ≠ expression</h1>
<p>Copyright tries to make a strict distinction between expression and idea, where the expression is copyrighted and the idea is not. In practice it’s difficult to separate the two, with the possible exception of conceptual art. If in conceptual art the idea constitutes the work of art, the concept in conceptual art can not be copyrighted. Only the form in which the concept is documented or fixated can be.</p>
</div>
<div id="section-5" class="slide section level1">
<h1><img src="https://i1.wp.com/we-make-money-not-art.com/wp-content/uploads/2017/06/33socl7b4e4_b.jpeg?resize=940%2C544"></h1>
</div>
<div id="for-example-as-far-as-we-know-its-piero-manzoni-who-came-up-with-the-idea-that-putting-an-up-side-down-plinth-on-the-ground-creates-a-pedestal-for-the-world." class="slide section level1">
<h1>For example, as far as we know, it’s Piero Manzoni who came up with the idea that putting an up side-down plinth on the ground creates a pedestal for the world.</h1>
<p>This idea is not protected; you can sculpt an up side-down plinth yourself. The thing you can not do is make exactly the same pedestal Manzoni did.</p>
</div>
<div id="copyright-getting-paid" class="slide section level1">
<h1>2.1 copyright — getting paid</h1>
<p>Once you are recognised as the author of something, copyright ensures you are the one who decides who can copy it.</p>
<p>Copyright law says nothing about getting paid, but the underlying assumption is that you can leverage copyright by asking for compensation from who ever wants to publish or republish your work.</p>
</div>
<div id="copyright-limiting-artistic-freedom" class="slide section level1">
<h1>2.1 copyright — limiting artistic freedom ?</h1>
<p>In the realm of copyright, other works and artistic heritage don’t really exist. Your inspiration is supposed to come from the realm of abstract ideas, not from the concrete forms that make up culture and its history.</p>
<p>This means that if your work incorporates existing text, images or sounds, you need to ask permission to the authors (or whichever publisher may now hold the rights) of the original pieces.</p>
<p>Wether or not they grant you that permission is entirely up to them. If they do grant you permission, it might be at the cost of a monetary exchange that could possibly be prohibitive.</p>
</div>
<div id="section-6" class="slide section level1">
<h1><img src="https://upload.wikimedia.org/wikipedia/en/c/c1/Endtroducingcover.jpg"></h1>
<p>Dj Shadow’s Endtroducing (2006)</p>
</div>
<div id="copyright-automatic-assignment-and-international-treaties" class="slide section level1">
<h1>2.1 copyright — automatic assignment and international treaties</h1>
<p>The Berne convention in 1886 achieved two key features for copyright:</p>
<ul>
<li>it introduced the concept that a copyright exists the moment a work is “fixed”</li>
</ul>
<p>↑ that is, written or recorded on some physical medium, its author is automatically entitled to all copyrights in the work</p>
<ul>
<li>copyright is (mostly) internationally recognised via the members know as Berne Union</li>
</ul>
<p>↑ https://en.wikipedia.org/wiki/List_of_parties_to_international_copyright_agreements</p>
</div>
<div id="copyright-length-of-application" class="slide section level1">
<h1>2.1 copyright — length of application</h1>
<p>Copyright last for a long time. It will extend past your lifetime and continue after your death. The Berne convention specifies a minimum of 50 years after the authors death, with copyright ending on the 1st of January of the year following this anniversary.</p>
<p>Most countries (including EU member states and the US) have pushed this to 70 years after the death of the author.</p>
<p>Mexico provides the longest protection: Andrés Henestrosa was born in 1906, published his first book at 22 and died in early 2008 at the age of 101. When his copyright expires, on 1 January 2109, <em>Los hombres que dispersó la danza</em> (1929) will have been under copyright in Mexico for 179 years.</p>
</div>
<div id="copyright-interesting-cases-photography" class="slide section level1">
<h1>2.1 copyright interesting cases: photography</h1>
<p>Once you’ve made a photograph or a drawing, no one can copy or even adapt it without permission. But that rule does not apply for the procedure or style with which you made the image.</p>
<p>In other words, it’s no problem to make a work in the style of another work, you just can’t copy.</p>
<p>So, when is a copy still a copy? If I make a drawing after a photograph, for example, do I still need permission? Most probably, yes. A drawing of a photograph is considered a derivative work as long as the form of the original is still recognizable in your copy.</p>
</div>
<div id="copyright-interesting-cases-graphic-design" class="slide section level1">
<h1>2.1 copyright interesting cases: graphic design</h1>
<p>Mixed licences are the norm:</p>
<p>In every design there are elements that you did not make yourself and whose copyright does not belong to you. There can be texts you did not write, photographs you did not take, typefaces you did not design. You need to make sure all the rights are secured before your design goes online or rolls off the press.</p>
</div>
<div id="copyright-interesting-cases-graphic-design-1" class="slide section level1">
<h1>2.1 copyright interesting cases: graphic design</h1>
<p>When someone pays you to make a design, that’s what they’re paying for, the service of making the design. Paying for your services does not automatically grant your clients copyright. The copyright remains yours. This might seem counterintuitive but, theoretically, the client will have to ask your permission to reproduce the design!</p>
<p><a href="https://eyeondesign.aiga.org/a-graphic-designers-guide-to-copyright/" class="uri">https://eyeondesign.aiga.org/a-graphic-designers-guide-to-copyright/</a></p>
</div>
<div id="copyright-interesting-cases-software" class="slide section level1">
<h1>2.1 copyright interesting cases: software</h1>
<p>Do you write code? In that case you write in a language readable to both machines and humans, which is what makes software unique as a cultural object.</p>
<p>Since code is now seen as a creative, cultural expression it receives the protection of copyright</p>
</div>
<div id="copyright-interesting-cases-typefaces" class="slide section level1">
<h1>2.1 copyright interesting cases: typefaces</h1>
<p>Drawings of faces originally couldn’t be copyrighted because they were specifically understood as utilitarian. But when they began being build in software, typefaces could be understood as texts describing shapes based off of creative decisions.</p>
<p>This means that digital typefaces, much like software, now can benefit from the protection of copyright.</p>
</div>
<div id="copyright-copyleft" class="slide section level1">
<h1>2.1 copyright &amp; copyleft</h1>
<p>A hack introduced by software developers when software began to be licenced.</p>
</div>
<div id="applying-licenses-creative-commons" class="slide section level1">
<h1>2.1 applying licenses — creative commons</h1>
<p><a href="https://creativecommons.org/" class="uri">https://creativecommons.org/</a></p>
</div>
