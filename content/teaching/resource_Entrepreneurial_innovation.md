Title: Entrepreneurial Innovation
Date: 10/2019
Template: slidy
Status: draft
Tags: resource

<!--

pandoc resource_Entrepreneurial_innovation.md --from=Markdown --to=Slidy -o resource_Entrepreneurial_innovation.md.slidy.md

paste to head of new doc:
Title: Entrepreneurial Innovation
Date: 10/2019
Template: slidy
Status: hidden
Tags: resource

-->

# Entrepreneurial Innovation

# outline
Before deciding *how to* innovate, let's investigate *whether* it's worth innovating with your current idea.

The following is a set of methods for thinking and questioning one's current or soon to be position in the market place.

# Customer needs

Entrepreneurial innovation is often helped by thinking about customer needs.

# 4 D's structure

Entrepreneurial innovation can be prepared using a 4 step method:

* **Discover** : Challenging the problem
* **Define** : Mapping your Journey
* **Develop** : Finding the Evidence
* **Deliver** : Creating Value

# 1 Discover

A great exercise is to think of an enterprises' or start ups' value proposition. Your value proposition should address what market you are targeting, what product or service you are delivering, how you are delivering it and why.

# Building blocks to Value Proposition:

There are three building blocks to question then build a strong value proposition:

* connect with customers
* differentiate from alternatives
* validate your claims

# Challenging the problem

We now must cross the three building blocks with three processes:

* Generating
* Delivering
* Reducing

# <img src="images/entrepreneurial-innovation/value-prop-blocks.png" style="width:100%;">

# The aim of this chart

The aim of value proposition chart is to answer the question: 'Why should we purchase your offering ?'. All of these questions let us identify if there are any weaknesses in our position and doings on the market.

It is a powerful tool to test your enterprise idea, or current enterprise practice. Your value proposition must be strong, across the board for you to make the most of your market. It is also a tool for motivating change in an existing organisation.

# If you know what your customers want, you can deliver what they need.

# 2 DEFINE : The pursuit of a customer:

It all starts with understanding who your customer is and *really* getting to know them.

# The pursuit of a customer is a journey

Simply identifying a product, service or method won't be enough enough; we need to understand :

* Aware: How does the customer become aware of you and your products
* Engage: How can your customer contact you and start the buying process
* Use: How can they buy and receive your product or service
* Continue: What happens after they buy from you, how do they continue their buying relationship with you ? (includes customer services, complaint and recovery)
* Leave: How does a customer leave you

# Understanding the customer journey

Once you understand YOUR customers journey, you can start to understand how you are different from your competitors. Once you know that, we are nearing an understanding of what your Unique selling point is.

# Unique Selling Point

is something that differentiates your product or service from your competitors

# 3 DEVELOP :

It is now time to start thinking about what your really know about your customers. We know that entrepreneurs have a reputation for being risk takers, but actually, they know their evidence inside and out.

# Finding the evidence for your target market

First think about one customer segment. That segment could be as small as one individual, but you really must know this individual inside and out. It is best if we can think about multiple different customer segments and confront each segment to the following claims.

# Method for finding evidence :

Question each following claim, and try to score them with 'High', 'Medium' or 'Low', plus back this up with evidence

# ... your ability to access key decision-makers:

Do you have the ability to access key decision-makers ? Will your communications, marketing and publicity reach decision makers in the target sector that you want ?

'High', 'Medium' or 'Low' ?

# ... the product / service 'fit' with customer requirement

Does your product really fit with the customer requirement ? Do you know the requirements ? Do you have their specification ? Have you interviewed or gathered feedback ?

# ... the readiness of customers to change to your solution

Are your potential customers ready to change to your solution ? How can you be sure ? Where is your evidence ?

# ... your ability to influence customers to change

Are your target customers willing to be influenced by you into changing to your product / service ?

# <img src="images/entrepreneurial-innovation/finding-evidence.png" style="width:100%;">

# 4 DELIVER

Think about the value you are adding. This can be adding value to the business (if it exists already and you are innovating within a established context), and / or value to the customer.

# Business growth

Business growth is often described as a life cycle.

# <img src="images/entrepreneurial-innovation/business-lifecycle.png" style="width:100%;">

# To remain relevant

a company needs to constantly check-in with their customers and with their strategy making sure they match, and making sure they are adding maximum value to both.
