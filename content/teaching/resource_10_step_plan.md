Title: 10 step plan on starting your own business
Date: 09/2019
Template: slidy
Status: draft

<!--

pandoc resource_10_step_plan.md --from=Markdown --to=Slidy -o resource_10_step_plan.md.slidy.md

paste to head of new doc:
Title: 10 step plan on starting your own business
Date: 09/2019
Template: slidy
Status: published

-->

# 10 step plan to think about your own business

# outline
If you’re thinking of starting a business, this 10 step plan will help you from developing and researching your ideas, learning basic business start-up skills and expanding your potential with marketing and financial planning knowledge.

# Step 1: Test Your Business Idea
* Have I got the right business skills?
* Think about who will buy your product or service.
* What is the benefit to them and how much will they pay ?

# Step 2: What About Market Research?
* From the outset market research is essential in helping you to identify your target market and customers.
* It will also help you to identify your competitors and how to compete effectively.
* Research is also effective in assessing demand for a new product or service.

# Step 3: What are Your Business Requirements?
* Have you considered the best location for the business?
* Identify your basic equipment requirements and costs.
* How many staff will you need to employ?
* Identify your overhead costs e.g. insurance.
* Can your business idea benefit from new technologies? e.g. by online selling.

# Step 4: What are your Investment Requirements?
* Identify all start-up and running costs associated with the business.
* Identify ways of financing your business venture.
* Seek financial support and benefit from direct referral to Government agencies.
* Seek advice on other sources of support e.g. Banks, Credit Unions, Microfinance Ireland, family support, other non-bank finance.

# Step 5: Developing your Marketing Strategy
* Marketing your business idea is a fundamental aspect of starting up.
* Research the most cost effective methods of marketing your business.
* Write your Marketing Plan.

# Step 6: Developing your Sales Plan
* How will you promote your product or service?
* Who and where is your target market (local, national, international)?
* What channels of distribution will be used?
* Determine your selling price and break-even point.

# Step 7: What is the Best Legal Structure for you?
* What type of company will allow you to make the best decisions for your business?

You could be a:

* Sole Trader
* Partnership
* Limited Company

# Step 8: Managing the Risks
* Starting a business is a big step to take.
* A new business can be exciting. However, it can also be risky.
* For some it means risking personal savings and secure employment.

# Step 9: Avoiding Unnecessary Risks.
* Register your business with the Companies Registration Office (CRO). Visit www.cro.ie
* Be aware of your tax obligations and register with your local Revenue office. Visit www.revenue.ie
* Be aware of other statutory obligations such as trading licences, planning permission, insurance, health and safety, patents, etc.
* Be aware of your responsibilities under employment rights legislation.

# Step 10: And Finally... Write your Business Plan
* Business Planning is fundamental to success in business – managing the company, generating sales and growing jobs.
* It is the key to getting things done and making things happen.
* The finished business plan can be used as an operating tool that will help you to make important decisions and manage your business effectively.
