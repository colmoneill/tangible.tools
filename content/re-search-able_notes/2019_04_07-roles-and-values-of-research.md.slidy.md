Title: ITC — role and value of research methodologies
Date: 2019/04/07
Template: slidy
Status: draft

<div class="slide section level1">

<p>Title: Roles and values of research and research methodologies in product / interaction design Date: 2019/04/07 Template: slidy Status: draft</p>
<!--
cd /home/colm/git/tangible.tools/content/re-search-able_notes
pandoc 2019_04_07-roles-and-values-of-research.md --from=Markdown --to=Slidy -o 2019_04_07-roles-and-values-of-research.md.slidy.md

paste to head of new doc:
Title: ITC — role and value of research methodologies
Date: 2019/04/07
Template: slidy
Status: published

 -->
</div>
<div id="discuss-the-role-and-value-of-research-and-research-methodologies-in-product-interaction-design" class="slide section level1">
<h1>Discuss the role and value of research and research methodologies in product / interaction design ?</h1>
<!--
To respond to the topic of this presentation, I think the most comprehensive way of working is to use an example, a case study. I'd like to talk about the Low Tech Magazine. For some context, the Low Tech Magazine is an Amsterdam based publication that prints regularly since 2012. The subject of the Low Tech Magazine is Low Tech. Low Tech exists as a counterpart to High Tech. We call sophisticated, cutting edge products High Tech, (meaning High technology) because they can be considered the most advanced technology available. The Low Tech Magazine isn't anti technology really it only exists to discursively consider what infrastructure, resources and dependencies advanced technology requires. The case study I'd like present is about the new Low Tech Magazine's website and it's infrastructure.

Like many print publications today, a magazines website is very important. If the print is the cornerstone of the publication, the website may be the hub. It's where most people find out about the magazine, where a lot of interaction happens between the publisher and the readers, and it also is a space that allows for shorter form, more experimental publishing. While the print magazines focused on Low Tech, the team wanted to reconsider their website with a similar looking glass, they began a research project with the question of how the Low Tech's Magazines website could better embody the Low Tech ideology.
 -->
</div>
<div id="low-tech-magazine" class="slide section level1">
<h1>LOW TECH MAGAZINE</h1>
<h3 id="solar-server-project">solar server project</h3>
<p>— a case study</p>
</div>
<div id="section" class="slide section level1">
<h1></h1>
<p><img src="../images/solar-low-tech-mag/mag_highres.jpg" /></p>
</div>
<div id="section-1" class="slide section level1">
<h1></h1>
<p><img src="../images/solar-low-tech-mag/triptique.png" /></p>
</div>
<div id="section-2" class="slide section level1">
<h1></h1>
<p><img src="../images/solar-low-tech-mag/CERN_Server_03.jpg" /></p>
</div>
<div id="section-3" class="slide section level1">
<h1></h1>
<p><img src="../images/solar-low-tech-mag/solar-powered-server-detail-2.png" /></p>
</div>
<div id="section-4" class="slide section level1">
<h1></h1>
<p><img src="../images/solar-low-tech-mag/sps_panel.png" /></p>
</div>
<div id="website-screenshot" class="slide section level1">
<h1>Website screenshot</h1>
<p><img src="../images/solar-low-tech-mag/low-tech1.png" /></p>
</div>
<div id="website-screenshot-1" class="slide section level1">
<h1>Website screenshot</h1>
<p><img src="../images/solar-low-tech-mag/low-tech-crop.png" /></p>
</div>
<div id="website-screenshot-2" class="slide section level1">
<h1>Website screenshot</h1>
<p><img src="../images/solar-low-tech-mag/low-tech2.png" /></p>
</div>
<div id="website-screenshot-3" class="slide section level1">
<h1>Website screenshot</h1>
<p><img src="../images/solar-low-tech-mag/low-tech0.png" /></p>
</div>
<div id="website-screenshot-4" class="slide section level1">
<h1>Website screenshot</h1>
<p><img src="../images/solar-low-tech-mag/low-tech3.png" /></p>
</div>
<div id="section-5" class="slide section level1">
<h1></h1>
<p><img src="../images/solar-low-tech-mag/whyoffline_screenshot.png" /></p>
</div>
<div id="research-methodologies-employed" class="slide section level1">
<h1>Research methodologies employed:</h1>
<ul>
<li>critical thinking</li>
<li>interdisciplinary (project ethos confronted to technical designs)</li>
<li>high risk taking</li>
<li>live experimentation</li>
<li>release early, release often</li>
<li>open communication</li>
</ul>
</div>
<div id="derived-value-from-the-research-itself" class="slide section level1">
<h1>Derived value from the research itself</h1>
<ul>
<li>By sharing the research process,</li>
<li>by allowing for critical thought and risk taking,</li>
<li>by experimenting with the live website,</li>
</ul>
<p>Low Tech Mag’s website and server infrastructure redesign directly exposed the value of open research methodologies for their end products, print and web.</p>
</div>
