Title: slides final assessment
Date: 2017/06/12
Template: slidy
Status: published

<div class="slide section level1">

<p>Title: final assessment Date: 2017/06/12 Template: slidy Status: draft</p>
</div>
<div id="my-aims" class="slide section level1">
<h1>My aims</h1>
<ul>
<li>develop a research practice</li>
<li>develop a writing practice</li>
<li>research the agency of my design practice</li>
<li>→ it's relationship to digital tools</li>
<li>→ it's relationship to digital litteracy</li>
<li>afford myself time to engage with media theory</li>
<li>develop python skills</li>
<li>develop javascript skills</li>
</ul>
</div>
<div id="the-areas-i-have-researched" class="slide section level1">
<h1>The areas I have researched</h1>
<ul>
<li>Interfaces, user interfaces, user interface design, Human Computer Interaction</li>
<li>drawing curved: on the nature of curvature → &lt;drawingcurved.osp.kitchen&gt;</li>
<li>Usability tests: conducting practical and spoken tests of how a 'user' expresses the ways by which actions are conducted</li>
<li>Languages of interface: mainly the written language of interface</li>
<li>Modes of address</li>
<li>Critical theory</li>
</ul>
</div>
<div id="brief-history-of-outputs-across-first-and-second-year" class="slide section level1">
<h1>Brief history of outputs across first and second year</h1>
</div>
<div id="trim-1" class="slide section level1">
<h1>trim 1</h1>
<p>concerns / intuitions / bold claims</p>
<p><a href="http://pzwart1.wdka.hro.nl/~colm/trimester-1_assessment/" class="uri">http://pzwart1.wdka.hro.nl/~colm/trimester-1_assessment/</a></p>
</div>
<div id="trim-2" class="slide section level1">
<h1>trim 2</h1>
<p>mapping networked structures, placing wedges at the interstices</p>
<p><a href="http://pzwart1.wdka.hro.nl/~colm/trimester-2_assessment/2016_04_05-trim2-assessment.html" class="uri">http://pzwart1.wdka.hro.nl/~colm/trimester-2_assessment/2016_04_05-trim2-assessment.html</a></p>
</div>
<div id="trim-3" class="slide section level1">
<h1>trim 3</h1>
<p>animated graphs</p>
<p><a href="http://pzwart1.wdka.hro.nl/~colm/trimester-3/out-8faster.mp4" class="uri">http://pzwart1.wdka.hro.nl/~colm/trimester-3/out-8faster.mp4</a></p>
</div>
<div id="trim-4" class="slide section level1">
<h1>trim 4</h1>
<ul>
<li>project proposals</li>
<li>thesis outline tangible.tools</li>
</ul>
</div>
<div id="trim-5" class="slide section level1">
<h1>trim 5</h1>
<ul>
<li>Continued use of tangible.tools as a notebook/sandbox</li>
<li>Thesis finalisation: tangible tools <a href="http://tangible.tools" class="uri">http://tangible.tools</a></li>
</ul>
</div>
<div id="thematic-seminars" class="slide section level1">
<h1>Thematic Seminars</h1>
<p><a href="https://pzwiki.wdka.nl/mediadesign/User:Colm" class="uri">https://pzwiki.wdka.nl/mediadesign/User:Colm</a></p>
<ul>
<li>[[Thematic-Letter Writing|Letter Writing]] with Florian Cramer and Goodiepal</li>
<li>[[Thematic-Chain Reactions|Chain Reactions]] with Richard Wright</li>
<li>[[User:Colm/TP-Chain-Reaction | Colm's page for this thematic -&gt; Thematic Seminar: Chain Reaction]]<br></li>
<li>[[Thematic-On Narrative]] with Kate Pullinger</li>
<li>[[Thematic-Navigating Borders and Contours|Navigating Borders and Contours]] with Tina Bastajian → [[Fuzzynarrators-report | project report page]]</li>
<li>[[Thematic-Making_It_Public|Making It Public]] with Annet Dekker and Andre Castro → [[Makingitpublic-report | project report page]]</li>
<li>[[Intro_Week-2015|Intro week presentations]]</li>
</ul>
</div>
<div id="essays" class="slide section level1">
<h1>essays</h1>
<p>drafts, ongoing repository <a href="https://git.xpub.nl/colm.drafts/files.html" class="uri">https://git.xpub.nl/colm.drafts/files.html</a></p>
</div>
<div id="bazaar" class="slide section level1">
<h1>bazaar</h1>
<ul>
<li><a href="https://github.com/colmoneill/prototypes/">crosspublish script</a>: writing in markdown, publishing to the wiki, pushing to multiple git remotes, making html versions for PZ1</li>
<li><a href="https://github.com/colmoneill/prototypes/blob/master/cross-publish/pelican-slidify.py">pelican-slidyfy script</a> writing W3c slidy presentations in .md publishing alt versions in html to tangible.tools</li>
<li><a href="https://github.com/colmoneill/WebExtensions">WebExtensions</a></li>
<li><a href="https://github.com/colmoneill/WebExtensions/tree/master/scrollometer">Scrollometer</a> scrollometer, finally a functionning webExtension</li>
</ul>
</div>
<div id="selected-final-outputs" class="slide section level1">
<h1>Selected final outputs</h1>
</div>
<div id="trim-6" class="slide section level1">
<h1>Trim 6</h1>
<p><a href="http://adversarial.interfaces.site" class="uri">http://adversarial.interfaces.site</a></p>
</div>
<div id="observation-of-informal-modes-of-address-phenomenon" class="slide section level1">
<h1>observation of informal modes of address phenomenon</h1>
<p>http://adversarial.interfaces.site/case-studies/informal-modes-of-address.html</p>
</div>
<div id="methods-of-obfuscation" class="slide section level1">
<h1>methods of obfuscation</h1>
<p>I believe that, the usage of informal modes of address in interfaces is a similar method to the idea of seamless interface design.</p>
<p>I don't think that these methods are actively used or thought of as entraping ways to lock users in, but the use of these methods, across all sorts of platforms and interfaces, together, have resulted in an image of the networked space as an unegociable geography.</p>
</div>
<div id="risk" class="slide section level1">
<h1>risk</h1>
<p>My opinion is that these methods, together, creating this image of totalised space, is quite risky. They create an uneccesary dependency for the user on easy interfaces, on software that smooths all experiences over.</p>
<p>This type of method, as helpful as it is trying to be, when it comes in conjunction with all of the other platforms and actors that chose similar techniques of ease and seamlessness actually slow down the processes by which an individual might be able to acquire a type of litteracy related to the infrastructures on which the sites/services/platforms exist.</p>
</div>
<div id="politics-ethics" class="slide section level1">
<h1>politics &amp; ethics</h1>
<p>A large portion of the time I spent in this school was on trying to explain concerns to people that might not feel similarly to me. This took differnet shapes over the two years, but I finally have found a space in which I think my reseach has agency.</p>
</div>
<div id="adversarial-design" class="slide section level1">
<h1>Adversarial design</h1>
<ul>
<li>Carl DiSalvo &gt; Through designerly means and forms, adversarial design evokes and engages political issues. Adversarial design is a type of political design.</li>
</ul>
<blockquote>
<p>Adversarial design is a kind of cultural production that does the work of agonism through the conceptualization and making of products and services and our experiences with them.</p>
</blockquote>
</div>
<div id="adversarial-design-adversarial-interfaces" class="slide section level1">
<h1>Adversarial design → adversarial interfaces</h1>
<p>http://adversarial.interfaces.site</p>
</div>
