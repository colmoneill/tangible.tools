Title: Roles and values of research and research methodologies in product / interaction design
Date: 2019/04/07
Template: slidy

<!--
cd /home/colm/git/tangible.tools/content/re-search-able_notes
pandoc 2019_04_07-roles-and-values-of-research.md --from=Markdown --to=Slidy -o 2019_04_07-roles-and-values-of-research.md.slidy.md

paste to head of new doc:
Title: ITC — role and value of research methodologies
Date: 2019/04/07
Template: slidy
Status: published

 -->

# Discuss the role and value of research and research methodologies in product / interaction design ?

<!--
To respond to the topic of this presentation, I think the most comprehensive way of working is to use an example, a case study. I'd like to talk about the Low Tech Magazine. For some context, the Low Tech Magazine is an Amsterdam based publication that prints regularly since 2012. The subject of the Low Tech Magazine is Low Tech. Low Tech exists as a counterpart to High Tech. We call sophisticated, cutting edge products High Tech, (meaning High technology) because they can be considered the most advanced technology available. The Low Tech Magazine isn't anti technology really it only exists to discursively consider what infrastructure, resources and dependencies advanced technology requires. The case study I'd like present is about the new Low Tech Magazine's website and it's infrastructure.

Like many print publications today, a magazines website is very important. If the print is the cornerstone of the publication, the website may be the hub. It's where most people find out about the magazine, where a lot of interaction happens between the publisher and the readers, and it also is a space that allows for shorter form, more experimental publishing. While the print magazines focused on Low Tech, the team wanted to reconsider their website with a similar looking glass, they began a research project with the question of how the Low Tech's Magazines website could better embody the Low Tech ideology.
 -->

# LOW TECH MAGAZINE
### solar server project
— a case study

#
![](../images/solar-low-tech-mag/mag_highres.jpg)

#
![](../images/solar-low-tech-mag/triptique.png)

#
![](../images/solar-low-tech-mag/CERN_Server_03.jpg)

#
![](../images/solar-low-tech-mag/solar-powered-server-detail-2.png)

#
![](../images/solar-low-tech-mag/sps_panel.png)

# Website screenshot
![](../images/solar-low-tech-mag/low-tech1.png)

# Website screenshot
![](../images/solar-low-tech-mag/low-tech-crop.png)

# Website screenshot
![](../images/solar-low-tech-mag/low-tech2.png)

# Website screenshot
![](../images/solar-low-tech-mag/low-tech0.png)

# Website screenshot
![](../images/solar-low-tech-mag/low-tech3.png)

#
![](../images/solar-low-tech-mag/whyoffline_screenshot.png)

# Research methodologies employed:
* critical thinking
* interdisciplinary (project ethos confronted to technical designs)
* high risk taking
* live experimentation
* release early, release often
* open communication

# Derived value from the research itself
* By sharing the research process,
* by allowing for critical thought and risk taking,
* by experimenting with the live website,

Low Tech Mag's website and server infrastructure redesign directly exposed the value of open research methodologies for their end products, print and web.
