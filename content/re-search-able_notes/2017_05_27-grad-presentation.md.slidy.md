Title: slides Adversarial interfacing
Date: 2017/05/27
Template: slidy
Status: draft

<div class="slide section level6">

<p>Title: Adversarial interfacing Date: 2017/05/27 Template: slidy Status: draft</p>
<div class="slide section level6">
<p>
slidyTitle: Adversarial interfacing Date: 2017/05/27 Template: slidy Status: draft
</p>
<div class="slide section level1">
<p>
Title: Adversarial interfacing Date: 2017/05/27 Template: slidy Status: draft
</p>
</div>
<div id="adversarial-interfacing" class="slide section level1">
<h1>
Adversarial interfacing:
</h1>
<ul>
<li>
Part 1: examples
</li>
<li>
Part 2: observations
</li>
<li>
Part 3: observations → project
</li>
<li>
Part 4: adversarial.interfacing.space site study
</li>
</ul>
</div>
<div id="examples" class="slide section level1">
<h1>
examples
</h1>
</div>
<div id="direct-address-sites-calling-me-by-my-first-name" class="slide section level1">
<h1>
direct address, sites calling me by my first name
</h1>
<div class="figure">
<p>
<img src="../images/grad-pres/hi-colm-amazon.png" />
</p>
</div>
<div class="figure">
<p>
<img src="../images/grad-pres/dont-worry-well-keep-it-private-crop.png" />
</p>
</div>
<div class="figure">
<p>
<img src="../images/grad-pres/5057840351_f14f986073_b.jpg" />
</p>
</div>
<div class="figure">
<p>
<img src="../images/grad-pres/tumblr-welcome-back.png" />
</p>
</div>
</div>
<div id="informality-to-obtain-consent" class="slide section level1">
<h1>
informality to obtain consent
</h1>
<div class="figure">
<p>
<img src="../images/grad-pres/tumblr-consent-sure.png" />
</p>
</div>
<div class="figure">
<p>
<img src="../images/grad-pres/twitter-sounds-good.png" />
</p>
</div>
</div>
<div id="informality-to-explain-a-point-of-view" class="slide section level1">
<h1>
informality to explain a point of view
</h1>
<div class="figure">
<p>
<img src="../images/grad-pres/Screenshot_2017-02-13_12-32-02.png" />
</p>
</div>
<div class="figure">
<p>
<img src="../images/grad-pres/daily-mash-crop.png" />
</p>
</div>
<div class="figure">
<p>
<img src="../images/grad-pres/nothardtoexplain-crop.png" />
</p>
</div>
</div>
<div id="informality-to-express-care" class="slide section level1">
<h1>
informality to express care
</h1>
<div class="figure">
<p>
<img src="../images/grad-pres/memo2.jpg" />
</p>
</div>
<div class="figure">
<p>
<img src="../images/grad-pres/splat-human-head.png" />
</p>
</div>
</div>
<div id="informality-to-detail-a-change-or-an-error" class="slide section level1">
<h1>
informality to detail a change or an error
</h1>
<div class="figure">
<p>
<img src="../images/grad-pres/tumblr-date-thee.png" />
</p>
</div>
<div class="figure">
<p>
<img src="../images/grad-pres/oh-fine.png" />
</p>
</div>
<div class="figure">
<p>
<img src="../images/grad-pres/highly-trained-monkeys.png" />
</p>
</div>
<div class="figure">
<p>
<img src="../images/grad-pres/digitalocean-awesome.png" />
</p>
</div>
</div>
<div id="observations-questions" class="slide section level1">
<h1>
observations &amp; questions
</h1>
</div>
<div id="what-is-the-need-for-this-informality-why-do-these-various-websites-and-products-want-me-to-be-intimate-with-them-why-do-they-want-me-to-be-so-enthusiastic-about-their-product-what-role-does-this-informality-play-for-the-designers-is-this-an-echo-of-the-way-the-makers-of-x-or-y-website-speak-among-themselves" class="slide section level1">
<h1>
What is the need for this informality? Why do these various websites and products want me to be intimate with them? Why do they want me to be so enthusiastic about their product? What role does this informality play for the designers? Is this an echo of the way the makers of x or y website speak among themselves?
</h1>
</div>
<div id="besides-who-exactly-is-talking-to-me-is-it-the-designers-is-the-the-product-manager-is-it-customer-support-my-practice-as-a-graphic-designer-keeps-on-reminding-me-that-inter-face-is-about-dialog.-i-know-who-is-on-my-end-but-who-is-on-the-other-end" class="slide section level1">
<h1>
Besides, who exactly is talking to me? Is it the designers? Is the the product manager? Is it customer support? My practice as a graphic designer keeps on reminding me that inter-face is about dialog. I know who is on my end, but who is on the other end?
</h1>
</div>
<div id="a-bit-of-a-reality-check-and-technical-knowledge-about-how-web-pages-are-templated-and-programatically-rendered-and-i-see-these-informal-addresses-not-as-the-pitch-of-a-bad-salesman-but-as-a-larger-symptom." class="slide section level1">
<h1>
A bit of a reality check, and technical knowledge about how web pages are templated and programatically rendered, and I see these informal addresses not as the pitch of a bad salesman, but as a larger symptom.
</h1>
</div>
<div id="we-are-in-an-age-of-mass-data-analytics.-big-datasets-only-getting-bigger-as-we-integrate-the-functions-of-the-internet-into-all-of-the-dimentions-of-life.-and-as-the-reach-of-data-analytics-grow-so-does-the-ability-to-treat-each-individual-uniquely-and-warmly." class="slide section level1">
<h1>
We are in an age of mass data analytics. Big datasets only getting bigger as we integrate the functions of the internet into all of the dimentions of life. And as the reach of data analytics grow, so does the ability to treat each individual uniquely and warmly.
</h1>
</div>
</div>
</div>
