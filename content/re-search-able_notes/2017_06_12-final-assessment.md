Title: final assessment
Date: 2017/06/12
Template: slidy
Status: draft

# My aims
* develop a research practice
* develop a writing practice
* research the agency of my design practice
* → it's relationship to digital tools
* → it's relationship to digital litteracy
* afford myself time to engage with media theory
* develop python skills
* develop javascript skills

# The areas I have researched
* Interfaces, user interfaces, user interface design, Human Computer Interaction
* drawing curved: on the nature of curvature → <drawingcurved.osp.kitchen>
* Usability tests: conducting practical and spoken tests of how a 'user' expresses the ways by which actions are conducted
* Languages of interface: mainly the written language of interface
* Modes of address
* Critical theory

# Brief history of outputs across first and second year
# trim 1
concerns / intuitions / bold claims

<http://pzwart1.wdka.hro.nl/~colm/trimester-1_assessment/>

# trim 2
mapping networked structures, placing wedges at the interstices

<http://pzwart1.wdka.hro.nl/~colm/trimester-2_assessment/2016_04_05-trim2-assessment.html>

# trim 3
animated graphs

<http://pzwart1.wdka.hro.nl/~colm/trimester-3/out-8faster.mp4>

# trim 4
* project proposals
* thesis outline tangible.tools

# trim 5
* Continued use of tangible.tools as a notebook/sandbox
* Thesis finalisation: tangible tools
<http://tangible.tools>

# Thematic Seminars
<https://pzwiki.wdka.nl/mediadesign/User:Colm>

* [[Thematic-Letter Writing|Letter Writing]] with Florian Cramer and Goodiepal
* [[Thematic-Chain Reactions|Chain Reactions]] with Richard Wright
* [[User:Colm/TP-Chain-Reaction | Colm's page for this thematic -> Thematic Seminar: Chain Reaction]]<br>
* [[Thematic-On Narrative]] with Kate Pullinger
* [[Thematic-Navigating Borders and Contours|Navigating Borders and Contours]] with Tina Bastajian → [[Fuzzynarrators-report | project report page]]
* [[Thematic-Making_It_Public|Making It Public]] with Annet Dekker and Andre Castro → [[Makingitpublic-report | project report page]]
* [[Intro_Week-2015|Intro week presentations]]

# essays
drafts, ongoing repository
<https://git.xpub.nl/colm.drafts/files.html>

# bazaar
* [crosspublish script](https://github.com/colmoneill/prototypes/): writing in markdown, publishing to the wiki, pushing to multiple git remotes, making html versions for PZ1
* [pelican-slidyfy script](https://github.com/colmoneill/prototypes/blob/master/cross-publish/pelican-slidify.py) writing W3c slidy presentations in .md publishing alt versions in html to tangible.tools
* [WebExtensions](https://github.com/colmoneill/WebExtensions)
* [Scrollometer](https://github.com/colmoneill/WebExtensions/tree/master/scrollometer) scrollometer, finally a functionning webExtension

# Selected final outputs

# Trim 6
<http://adversarial.interfaces.site>

# observation of informal modes of address phenomenon
http://adversarial.interfaces.site/case-studies/informal-modes-of-address.html

# methods of obfuscation
I believe that, the usage of informal modes of address in interfaces is a similar method to the idea of seamless interface design.

I don't think that these methods are actively used or thought of as entraping ways to lock users in, but the use of these methods, across all sorts of platforms and interfaces, together, have resulted in an image of the networked space as an unegociable geography.

# risk

My opinion is that these methods, together, creating this image of totalised space, is quite risky. They create an uneccesary dependency for the user on easy interfaces, on software that smooths all experiences over.

This type of method, as helpful as it is trying to be, when it comes in conjunction with all of the other platforms and actors that chose similar techniques of ease and seamlessness actually slow down the processes by which an individual might be able to acquire a type of litteracy related to the infrastructures on which the sites/services/platforms exist.

# politics & ethics

A large portion of the time I spent in this school was on trying to explain concerns to people that might not feel similarly to me. This took differnet shapes over the two years, but I finally have found a space in which I think my reseach has agency.

# Adversarial design

* Carl DiSalvo
> Through designerly means and forms, adversarial design evokes and engages political issues. Adversarial design is a type of political design.

> Adversarial design is a kind of cultural production that does the work of agonism through the conceptualization and making of products and services and our experiences with them.

# Adversarial design → adversarial interfaces

http://adversarial.interfaces.site
