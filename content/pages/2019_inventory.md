Title: inventory
Date: 2019/09/10

# pages inventory

* [styleguide](/pages/styleguide.html)
* [re_searchable notes](/category/re-search-able_notes.html)
* [modes of address](/category/modes_of_address.html)
* [thesis (tangible tools)](/category/thesis.html)
